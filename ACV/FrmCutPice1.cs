﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace NaalwarWeightCapture
{
    public partial class FrmCutPice1 : Form
    {
        private TextBox focusedTextbox = null;
        public FrmCutPice1()
        {
            InitializeComponent();
            this.FormClosing += FrmCutPice_FormClosing;
            touchScreen1.OnUserControlButtonClicked += new TouchScreen.ButtonClickedEventHandler(touchScreen1_OnUserControlButtonClicked);
        }
        DataTable dtBarcode = new DataTable();
        SQLDBHelper db = new SQLDBHelper();
        SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnStr"].ConnectionString);
        int BUid = 0;
        int YearId, MonthId, LastNo;
        private void touchScreen1_OnUserControlButtonClicked(object sender, EventArgs e)
        {
            try
            {
                Button b = (Button)sender;
                if (focusedTextbox != null)
                {
                    if (b.Text == "Back")
                    {
                        if (focusedTextbox.Text.Length > 1)
                        {
                            focusedTextbox.Text = focusedTextbox.Text.Substring(0, focusedTextbox.Text.Length - 1);
                        }
                        else
                        {
                            focusedTextbox.Text = string.Empty;
                        }
                    }
                    else if (b.Text == "Clear")
                    {
                        focusedTextbox.Text = string.Empty;
                        focusedTextbox.Focus();
                    }
                    else if (b.Text == "Enter")
                    {
                        this.SelectNextControl(focusedTextbox, true, true, true, true);
                    }
                    else
                    {
                        if (MyGlobal.bTouch)
                            focusedTextbox.Text = b.Text;
                        else
                        {
                            MyGlobal.bTouch = false;
                            focusedTextbox.Text += b.Text;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Infromation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void FrmCutPice_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                if (e.CloseReason == CloseReason.UserClosing)
                {
                    DialogResult result = MessageBox.Show("Do you really back to the application?", "Closing", MessageBoxButtons.YesNo, MessageBoxIcon.Stop);
                    if (result == DialogResult.Yes)
                    {
                        this.Hide();
                        FrmQc qc = new FrmQc();
                        qc.Show();
                    }
                    else
                    {
                        e.Cancel = true;
                    }
                }
                else
                {
                    e.Cancel = true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Infromation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void FrmCutPice_Load(object sender, EventArgs e)
        {
            grCutpice.Width = this.Width - 20;
            grCutpice.Height = this.Height - 70;
            panelMain.Location = new Point(ClientSize.Width / 2 - panelMain.Size.Width / 2, ClientSize.Height / 2 - panelMain.Size.Height / 2);
            panelMain.Anchor = AnchorStyles.None;
            textBox1.Focus();
            LoadTitle();
            dtBarcode = db.GetData(CommandType.StoredProcedure, "SP_GetQCBarcode");
            AutoSortNo();
        }

        private void PrintBarcode(string Barcode)
        {
            try
            {
                FileStream fs = new FileStream(Application.StartupPath + "\\QCBarcode.txt", FileMode.Truncate, FileAccess.Write);
                fs.Close();
                for (int i = 0; i < dtBarcode.Rows.Count; i++)
                {
                    string Var = dtBarcode.Rows[i]["VarType"].ToString();
                    if (Var == "Static")
                    {
                        File.AppendAllText(Application.StartupPath + "\\QCBarcode.txt", dtBarcode.Rows[i]["BCodeFormat"].ToString() + Environment.NewLine);
                    }
                    else
                    {
                        int LineNUmber = Convert.ToInt32(dtBarcode.Rows[i]["LineNumber"].ToString());
                        if (LineNUmber == 10)
                        {
                            string text = dtBarcode.Rows[i]["BCodeFormat"].ToString() + "\"" + Barcode + "\"";
                            File.AppendAllText(Application.StartupPath + "\\QCBarcode.txt", text + Environment.NewLine);
                        }
                        else if (LineNUmber == 12)
                        {
                            string text = dtBarcode.Rows[i]["BCodeFormat"].ToString() + "\"" + Barcode + "\"";
                            File.AppendAllText(Application.StartupPath + "\\QCBarcode.txt", text + Environment.NewLine);
                        }
                        else if (LineNUmber == 19)
                        {
                            string text = dtBarcode.Rows[i]["BCodeFormat"].ToString() + "\"" + txtSortNo.Text + "\"";
                            File.AppendAllText(Application.StartupPath + "\\QCBarcode.txt", text + Environment.NewLine);
                        }
                        else if (LineNUmber == 20)
                        {
                            string text = dtBarcode.Rows[i]["BCodeFormat"].ToString() + "\"" + (Convert.ToDecimal(txtWeight.Text) / Convert.ToDecimal(txtWeight.Tag)).ToString("0") + "\"";
                            File.AppendAllText(Application.StartupPath + "\\QCBarcode.txt", text + Environment.NewLine);
                        }
                        else if (LineNUmber == 21)
                        {
                            string text = dtBarcode.Rows[i]["BCodeFormat"].ToString() + "\"" + Convert.ToDecimal(txtWeight.Text).ToString("0") + "\"";
                            File.AppendAllText(Application.StartupPath + "\\QCBarcode.txt", text + Environment.NewLine);
                        }
                        else if (LineNUmber == 24)
                        {
                            string text = dtBarcode.Rows[i]["BCodeFormat"].ToString() + "\"" + Convert.ToDateTime(dtpDate.Text).ToString("dd.MM.yyyy") + "\"";
                            File.AppendAllText(Application.StartupPath + "\\QCBarcode.txt", text + Environment.NewLine);
                        }
                        else if (LineNUmber == 27)
                        {
                            string text = dtBarcode.Rows[i]["BCodeFormat"].ToString() + "\"" + lblMendorName.Text + "\"";
                            File.AppendAllText(Application.StartupPath + "\\QCBarcode.txt", text + Environment.NewLine);
                        }
                    }
                }
                Process.Start(Application.StartupPath + "\\BPQc.bat");
                //MessageBox.Show("Printed", "Infromation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                //Process.Start(Application.StartupPath + "\\BarPrint.bat");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void chckNumpad_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult result = MessageBox.Show("Do you really back to the application?", "Closing", MessageBoxButtons.YesNo, MessageBoxIcon.Stop);
                if (result == DialogResult.Yes)
                {
                    this.Hide();
                    FrmQc qc = new FrmQc();
                    qc.Show();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void txtbarcode_Enter(object sender, EventArgs e)
        {
            focusedTextbox = (TextBox)sender;
        }

        private void txtMeter_Enter(object sender, EventArgs e)
        {
            focusedTextbox = (TextBox)sender;
        }

        private void txtWeight_Enter(object sender, EventArgs e)
        {
            focusedTextbox = (TextBox)sender;
        }

        private void cmbNoofPices_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                //if (cmbNoofPices.SelectedIndex != -1)
                //{
                //    if (txtbarcode.Text != string.Empty)
                //    {
                //        cmbBarcodeNoofPices.Items.Clear();
                //        if (cmbNoofPices.SelectedIndex != -1)
                //        {
                //            int cnt = Convert.ToInt32(cmbNoofPices.Text);
                //            for (int i = 0; i < cnt; i++)
                //            {
                //                cmbBarcodeNoofPices.Items.Add(txtbarcode.Text + "/" + (i + 1).ToString("D1"));
                //            }
                //        }
                //        SqlParameter[] para = { new SqlParameter("@TotalMeters", txtbarcode.Text) };
                //        DataTable dt = db.GetData(CommandType.StoredProcedure, "SP_GetSortDetailId", para);
                //        txtbarcode.Tag = dt.Rows[0]["Id"].ToString();
                //        cmbBarcodeNoofPices.SelectedIndex = 0;
                //        txtMeter.Focus();
                //    }
                //    else
                //    {
                //        MessageBox.Show("Enter Barcode", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                //        txtbarcode.Focus();
                //    }
                //}
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            try
            {
                SqlParameter[] para = { new SqlParameter("@BarType", "Cutpice") };
                DataTable dt = db.GetData(CommandType.StoredProcedure, "SP_BarcodePrint", para);
                YearId = Convert.ToInt32(dt.Rows[0]["YerarId"].ToString());
                MonthId = Convert.ToInt32(dt.Rows[0]["MonthId"].ToString());
                LastNo = Convert.ToInt32(dt.Rows[0]["LastSNo"].ToString());
                BUid = Convert.ToInt32(dt.Rows[0]["Uid"].ToString());
                //string barcode = cmbBarcodeNoofPices.Text;
                LastNo += 1;
                string yu = LastNo.ToString("D4");
                string Barcode = YearId.ToString() + MonthId.ToString("D2") + BUid + yu;
               if (txtMeter.Text != string.Empty && txtWeight.Text != string.Empty && txtSortNo.Text != string.Empty)
                {
                    bool entryFound = false;
                    foreach (DataGridViewRow row1 in DataGridCutPices.Rows)
                    {
                        object val2 = row1.Cells[1].Value;
                        object val3 = row1.Cells[2].Value;
                        object val4 = row1.Cells[3].Value;
                        if (val2 != null && val2.ToString() == txtSortNo.Text && val3 != null && val3.ToString() == txtMeter.Text && val4 != null && val4.ToString() == txtWeight.Text)
                        {
                            MessageBox.Show("Entry already exist", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            entryFound = true;
                            break;
                        }
                    }
                    if (!entryFound)
                    {
                        decimal Weight = 0;
                        if (txtNetWeight.Text == string.Empty)
                        {
                            txtNetWeight.Text = "0";
                        }
                        Weight = Convert.ToDecimal(txtNetWeight.Text);
                        DataGridViewRow row = (DataGridViewRow)DataGridCutPices.Rows[0].Clone();
                        row.Cells[0].Value = DataGridCutPices.Rows.Count;
                        row.Cells[1].Value = Barcode;
                        row.Cells[2].Value = (Convert.ToDecimal(txtWeight.Text) / Convert.ToDecimal(txtWeight.Tag)).ToString("0");
                        row.Cells[3].Value = txtWeight.Text;
                        row.Cells[4].Value = "";
                        row.Cells[5].Value = txtSortNo.Text;
                        DataGridCutPices.Rows.Add(row);
                        txtNetWeight.Text = (Weight + Convert.ToDecimal(txtWeight.Text)).ToString();
                    }
                }
                else
                {
                    MessageBox.Show("Enter Sort No,Weight", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtSortNo.Focus();
                }
                SqlParameter[] para2 = { new SqlParameter("@BarType", "Cutpice"), new SqlParameter("@LastSNo", LastNo) };
                db.ExecuteQuery(CommandType.StoredProcedure, "SP_UpdateBarcode", para2, conn);
                PrintBarcode(Barcode);
                txtSortNo.Text = string.Empty;
                txtWeight.Text = string.Empty;
                //cmbBarcodeNoofPices.Items.Remove(barcode);
                //if (cmbBarcodeNoofPices.Items.Count != 0)
                //{
                //    cmbBarcodeNoofPices.SelectedIndex = 0;
                //}
                //txtMeter.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        protected void LoadWeight()
        {
            try
            {
                decimal Weight = 0;
                for (int i = 0; i < DataGridCutPices.Rows.Count - 1; i++)
                {
                    Weight += Convert.ToDecimal(DataGridCutPices.Rows[i].Cells[3].Value);
                }
                txtNetWeight.Text = Weight.ToString();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void grCutpice_Enter(object sender, EventArgs e)
        {

        }

        protected void LoadTitle()
        {
            try
            {
                DataGridCutPices.DataSource = null;
                DataGridCutPices.AutoGenerateColumns = false;
                DataGridCutPices.ColumnCount = 6;
                DataGridCutPices.Columns[0].Name = "SlNo";
                DataGridCutPices.Columns[0].Name = "SlNo";

                DataGridCutPices.Columns[1].Name = "QCBarcode";
                DataGridCutPices.Columns[1].Name = "QCBarcode";
                DataGridCutPices.Columns[1].Width = 140;

                DataGridCutPices.Columns[2].Name = "Length";
                DataGridCutPices.Columns[2].Name = "Length";
                DataGridCutPices.Columns[2].Width = 100;

                DataGridCutPices.Columns[3].Name = "Weight";
                DataGridCutPices.Columns[3].Name = "Weight";
                DataGridCutPices.Columns[3].Width = 100;
                DataGridCutPices.Columns[3].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                DataGridCutPices.Columns[4].Name = "Barcode";
                DataGridCutPices.Columns[4].Name = "Barcode";

                DataGridCutPices.Columns[5].Name = "SortNo";
                DataGridCutPices.Columns[5].Name = "SortNo";
                DataGridCutPices.Columns[5].Width = 260;
                DataGridCutPices.Columns[4].Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void DataGridCutPices_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            for (int i = 0; i < DataGridCutPices.Rows.Count - 1; i++)
            {
                DataGridCutPices.Rows[i].Cells[0].Value = i + 1;
            }
        }

        private void bntPrintBarcode_Click(object sender, EventArgs e)
        {
            try
            {
                if (DataGridCutPices.Rows.Count != 0)
                {
                    int Index = DataGridCutPices.SelectedCells[0].RowIndex;
                    FileStream fs = new FileStream(Application.StartupPath + "\\QCBarcode.txt", FileMode.Truncate, FileAccess.Write);
                    fs.Close();
                    for (int i = 0; i < dtBarcode.Rows.Count; i++)
                    {
                        string Var = dtBarcode.Rows[i]["VarType"].ToString();
                        if (Var == "Static")
                        {
                            File.AppendAllText(Application.StartupPath + "\\QCBarcode.txt", dtBarcode.Rows[i]["BCodeFormat"].ToString() + Environment.NewLine);
                        }
                        else
                        {
                            int LineNUmber = Convert.ToInt32(dtBarcode.Rows[i]["LineNumber"].ToString());
                            if (LineNUmber == 10)
                            {
                                string text = dtBarcode.Rows[i]["BCodeFormat"].ToString() + "\"" + DataGridCutPices.Rows[Index].Cells[1].Value + "\"";
                                File.AppendAllText(Application.StartupPath + "\\QCBarcode.txt", text + Environment.NewLine);
                            }
                            else if (LineNUmber == 12)
                            {
                                string text = dtBarcode.Rows[i]["BCodeFormat"].ToString() + "\"" + DataGridCutPices.Rows[Index].Cells[1].Value + "\"";
                                File.AppendAllText(Application.StartupPath + "\\QCBarcode.txt", text + Environment.NewLine);
                            }
                            else if (LineNUmber == 19)
                            {
                                string text = dtBarcode.Rows[i]["BCodeFormat"].ToString() + "\"" + DataGridCutPices.Rows[Index].Cells[5].Value.ToString() + "\"";
                                File.AppendAllText(Application.StartupPath + "\\QCBarcode.txt", text + Environment.NewLine);
                            }
                            else if (LineNUmber == 20)
                            {
                                string text = dtBarcode.Rows[i]["BCodeFormat"].ToString() + "\"" + Convert.ToDecimal(DataGridCutPices.Rows[Index].Cells[2].Value).ToString("0") + "\"";
                                File.AppendAllText(Application.StartupPath + "\\QCBarcode.txt", text + Environment.NewLine);
                            }
                            else if (LineNUmber == 21)
                            {
                                string text = dtBarcode.Rows[i]["BCodeFormat"].ToString() + "\"" + Convert.ToDecimal(DataGridCutPices.Rows[Index].Cells[3].Value).ToString("0") + "\"";
                                File.AppendAllText(Application.StartupPath + "\\QCBarcode.txt", text + Environment.NewLine);
                            }
                            else if (LineNUmber == 24)
                            {
                                string text = dtBarcode.Rows[i]["BCodeFormat"].ToString() + "\"" + Convert.ToDateTime(dtpDate.Text).ToString("dd.MM.yyyy") + "\"";
                                File.AppendAllText(Application.StartupPath + "\\QCBarcode.txt", text + Environment.NewLine);
                            }
                            else if (LineNUmber == 27)
                            {
                                string text = dtBarcode.Rows[i]["BCodeFormat"].ToString() + "\"" + lblMendorName.Text + "\"";
                                File.AppendAllText(Application.StartupPath + "\\QCBarcode.txt", text + Environment.NewLine);
                            }
                        }
                    }
                    Process.Start(Application.StartupPath + "\\BPQc.bat");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtNetWeight.Text != string.Empty)
                {
                    if (DataGridCutPices.Rows.Count > 1)
                    {
                        SqlParameter[] para = {
                            new SqlParameter("@QCBarcode",DBNull.Value),
                            new SqlParameter("@QCDate",Convert.ToDateTime(dtpDate.Text)),
                            new SqlParameter("@TotalWeight",Convert.ToDecimal(txtNetWeight.Text)),
                            new SqlParameter("@RefUid",txtSortNo.Tag),
                            new SqlParameter("@Refbarcode",""),
                            new SqlParameter("@ReturnMessage",SqlDbType.Int),
                            new SqlParameter("@MentorName",lblMendorName.Text)
                        };
                        para[5].Direction = ParameterDirection.Output;
                        int id = db.ExecuteQuery(CommandType.StoredProcedure, "SP_QcM", para, 5);

                        for (int i = 0; i < DataGridCutPices.Rows.Count - 1; i++)
                        {
                            SqlParameter[] paraDet = {
                                new SqlParameter("@QCUid",id),
                                new SqlParameter("@QCBarcode",DataGridCutPices.Rows[i].Cells[1].Value.ToString()),
                                new SqlParameter("@Meters",Convert.ToDecimal(DataGridCutPices.Rows[i].Cells[2].Value.ToString())),
                                new SqlParameter("@Weight",Convert.ToDecimal(DataGridCutPices.Rows[i].Cells[3].Value.ToString())),
                                new SqlParameter("@RefBarcode",Convert.ToDecimal(DataGridCutPices.Rows[i].Cells[4].Value.ToString()))
                            };
                            db.ExecuteQuery(CommandType.StoredProcedure, "SP_QCDet", paraDet);
                        }
                    }
                }
                else
                {
                    MessageBox.Show("Enter net Weight", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtNetWeight.Focus();
                }
                MessageBox.Show("Record has been Successfully Saved", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                ClearControl();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        protected void ClearControl()
        {
            txtSortNo.Text = string.Empty;
            txtWeight.Text = string.Empty;
            txtMeter.Text = string.Empty;
            //cmbNoofPices.SelectedIndex = -1;
            //cmbBarcodeNoofPices.Items.Clear();
            //lblSortNo.Text = string.Empty;
            txtNetWeight.Text = string.Empty;
            DataGridCutPices.Rows.Clear();
            textBox1.Text = string.Empty;
            lblMendorName.Text = string.Empty;
            txtSortNo.Focus();
        }

        private void txtbarcode_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                //if (e.KeyCode == Keys.Enter)
                //{
                //    SqlParameter[] para = {
                //    new SqlParameter("@Barcode",txtbarcode.Text)
                //};
                //    DataTable dt = db.GetData(CommandType.StoredProcedure, "SP_GetSortNoRollNo", para);
                //    lblSortNo.Text = dt.Rows[0]["SortNo"].ToString();
                //    cmbNoofPices.SelectedIndex = 0;
                //    cmbNoofPices.Focus();
                //}
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void txtMeter_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    txtWeight.Focus();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void txtWeight_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    btnOK_Click(sender, e);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            try
            {
                ClearControl();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void textBox1_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    SqlParameter[] para = {
                        new SqlParameter("@TypeM_Uid",28),
                        new SqlParameter("@Active",1),
                        new SqlParameter("@Code",textBox1.Text)
                    };
                    DataTable dt = db.GetData(CommandType.StoredProcedure, "SP_GetGeneralM", para);
                    lblMendorName.Text = dt.Rows[0]["f2"].ToString();
                    textBox1.Tag = dt.Rows[0]["Uid"].ToString();
                    txtSortNo.Focus();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void textBox1_Enter(object sender, EventArgs e)
        {
            focusedTextbox = (TextBox)sender;
        }

        public void AutoSortNo()
        {
            AutoCompleteStringCollection coll = new AutoCompleteStringCollection();
            SqlDataAdapter da = new SqlDataAdapter("Select SortNo from SortDet Where SortNo <> ''", conn);
            DataTable dt = new DataTable();
            da.Fill(dt);
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    coll.Add(dt.Rows[i]["SortNo"].ToString());
                }
            }
            else
            {
                MessageBox.Show("Name not found");
            }
            txtSortNo.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            txtSortNo.AutoCompleteSource = AutoCompleteSource.CustomSource;
            txtSortNo.AutoCompleteCustomSource = coll;
        }

        private void txtSortNo_Leave(object sender, EventArgs e)
        {
            try
            {
                if (txtSortNo.Text == string.Empty)
                {
                    MessageBox.Show("Sort no can't Empty", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtSortNo.Focus();
                }
                else
                {
                    SqlDataAdapter da = new SqlDataAdapter("Select Uid,SortNo,WtMtr from SortDet Where SortNo ='" + txtSortNo.Text + "'", conn);
                    DataTable dt = new DataTable();
                    da.Fill(dt);
                    txtSortNo.Tag = dt.Rows[0]["Uid"].ToString();
                    txtWeight.Tag = dt.Rows[0]["WtMtr"].ToString();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }
    }
}
