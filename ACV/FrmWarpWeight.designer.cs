﻿namespace NaalwarWeightCapture
{
    partial class FrmWarpWeight
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.grWeight = new System.Windows.Forms.GroupBox();
            this.touchScreen1 = new NaalwarWeightCapture.TouchScreen();
            this.DataGridWarpWeight = new System.Windows.Forms.DataGridView();
            this.dtpDate = new System.Windows.Forms.DateTimePicker();
            this.btnReset = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.txtBeamNo = new System.Windows.Forms.TextBox();
            this.btnBack = new System.Windows.Forms.Button();
            this.bntPrintBarcode = new System.Windows.Forms.Button();
            this.txtWeight = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtTotalDiff = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txttotalTar = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtTotalNet = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtTotalActual = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.chckNumpad = new System.Windows.Forms.CheckBox();
            this.chckbatch = new System.Windows.Forms.CheckBox();
            this.grWeight.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridWarpWeight)).BeginInit();
            this.SuspendLayout();
            // 
            // grWeight
            // 
            this.grWeight.AutoSize = true;
            this.grWeight.Controls.Add(this.touchScreen1);
            this.grWeight.Controls.Add(this.DataGridWarpWeight);
            this.grWeight.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grWeight.Location = new System.Drawing.Point(12, 8);
            this.grWeight.Name = "grWeight";
            this.grWeight.Size = new System.Drawing.Size(1218, 581);
            this.grWeight.TabIndex = 1;
            this.grWeight.TabStop = false;
            // 
            // touchScreen1
            // 
            this.touchScreen1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.touchScreen1.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.touchScreen1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.touchScreen1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.touchScreen1.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.touchScreen1.Location = new System.Drawing.Point(988, 15);
            this.touchScreen1.Margin = new System.Windows.Forms.Padding(4);
            this.touchScreen1.Name = "touchScreen1";
            this.touchScreen1.Size = new System.Drawing.Size(224, 392);
            this.touchScreen1.TabIndex = 33;
            this.touchScreen1.Visible = false;
            // 
            // DataGridWarpWeight
            // 
            this.DataGridWarpWeight.AllowUserToAddRows = false;
            this.DataGridWarpWeight.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DataGridWarpWeight.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.DataGridWarpWeight.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridWarpWeight.Location = new System.Drawing.Point(8, 19);
            this.DataGridWarpWeight.Name = "DataGridWarpWeight";
            this.DataGridWarpWeight.ReadOnly = true;
            this.DataGridWarpWeight.RowHeadersVisible = false;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DataGridWarpWeight.RowsDefaultCellStyle = dataGridViewCellStyle2;
            this.DataGridWarpWeight.RowTemplate.Height = 30;
            this.DataGridWarpWeight.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DataGridWarpWeight.Size = new System.Drawing.Size(1093, 537);
            this.DataGridWarpWeight.TabIndex = 8;
            this.DataGridWarpWeight.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.DataGridWarpWeight_CellMouseClick);
            this.DataGridWarpWeight.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridWarpWeight_CellValueChanged);
            // 
            // dtpDate
            // 
            this.dtpDate.CalendarFont = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpDate.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpDate.Location = new System.Drawing.Point(1183, 54);
            this.dtpDate.Name = "dtpDate";
            this.dtpDate.Size = new System.Drawing.Size(157, 31);
            this.dtpDate.TabIndex = 31;
            this.dtpDate.ValueChanged += new System.EventHandler(this.DtpDate_ValueChanged);
            // 
            // btnReset
            // 
            this.btnReset.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnReset.BackColor = System.Drawing.Color.Yellow;
            this.btnReset.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReset.Location = new System.Drawing.Point(1151, 386);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(188, 85);
            this.btnReset.TabIndex = 15;
            this.btnReset.Text = "Refresh";
            this.btnReset.UseVisualStyleBackColor = false;
            this.btnReset.Click += new System.EventHandler(this.BtnReset_Click);
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.BackColor = System.Drawing.Color.Lime;
            this.btnSave.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.Location = new System.Drawing.Point(1151, 298);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(188, 85);
            this.btnSave.TabIndex = 14;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.BtnSave_Click);
            // 
            // txtBeamNo
            // 
            this.txtBeamNo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtBeamNo.Font = new System.Drawing.Font("Calibri", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBeamNo.Location = new System.Drawing.Point(1147, 125);
            this.txtBeamNo.Multiline = true;
            this.txtBeamNo.Name = "txtBeamNo";
            this.txtBeamNo.Size = new System.Drawing.Size(193, 66);
            this.txtBeamNo.TabIndex = 13;
            this.txtBeamNo.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // btnBack
            // 
            this.btnBack.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnBack.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.btnBack.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBack.Location = new System.Drawing.Point(1151, 563);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(188, 85);
            this.btnBack.TabIndex = 17;
            this.btnBack.Text = "Back";
            this.btnBack.UseVisualStyleBackColor = false;
            this.btnBack.Click += new System.EventHandler(this.BtnBack_Click);
            // 
            // bntPrintBarcode
            // 
            this.bntPrintBarcode.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.bntPrintBarcode.BackColor = System.Drawing.Color.SpringGreen;
            this.bntPrintBarcode.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bntPrintBarcode.Location = new System.Drawing.Point(1151, 474);
            this.bntPrintBarcode.Name = "bntPrintBarcode";
            this.bntPrintBarcode.Size = new System.Drawing.Size(188, 85);
            this.bntPrintBarcode.TabIndex = 16;
            this.bntPrintBarcode.Text = "Print Barcode";
            this.bntPrintBarcode.UseVisualStyleBackColor = false;
            this.bntPrintBarcode.Click += new System.EventHandler(this.BntPrintBarcode_Click);
            // 
            // txtWeight
            // 
            this.txtWeight.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtWeight.Font = new System.Drawing.Font("Calibri", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtWeight.Location = new System.Drawing.Point(1147, 228);
            this.txtWeight.Name = "txtWeight";
            this.txtWeight.Size = new System.Drawing.Size(193, 66);
            this.txtWeight.TabIndex = 12;
            this.txtWeight.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtWeight.Enter += new System.EventHandler(this.TxtWeight_Enter);
            this.txtWeight.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtWeight_KeyDown);
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Calibri", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(1209, 194);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(93, 33);
            this.label6.TabIndex = 19;
            this.label6.Text = "Weight";
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Calibri", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(1209, 88);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(115, 33);
            this.label5.TabIndex = 18;
            this.label5.Text = "Beam No";
            // 
            // txtTotalDiff
            // 
            this.txtTotalDiff.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotalDiff.Location = new System.Drawing.Point(975, 604);
            this.txtTotalDiff.Name = "txtTotalDiff";
            this.txtTotalDiff.Size = new System.Drawing.Size(94, 26);
            this.txtTotalDiff.TabIndex = 30;
            this.txtTotalDiff.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtTotalDiff.Visible = false;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(975, 631);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(51, 18);
            this.label12.TabIndex = 29;
            this.label12.Text = "Diif Wt";
            this.label12.Visible = false;
            // 
            // txttotalTar
            // 
            this.txttotalTar.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txttotalTar.Location = new System.Drawing.Point(674, 604);
            this.txttotalTar.Name = "txttotalTar";
            this.txttotalTar.Size = new System.Drawing.Size(94, 26);
            this.txttotalTar.TabIndex = 28;
            this.txttotalTar.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txttotalTar.Visible = false;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(674, 631);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(45, 18);
            this.label11.TabIndex = 27;
            this.label11.Text = "Tar.Wt";
            this.label11.Visible = false;
            // 
            // txtTotalNet
            // 
            this.txtTotalNet.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotalNet.Location = new System.Drawing.Point(775, 604);
            this.txtTotalNet.Name = "txtTotalNet";
            this.txtTotalNet.Size = new System.Drawing.Size(94, 26);
            this.txtTotalNet.TabIndex = 26;
            this.txtTotalNet.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtTotalNet.Visible = false;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(775, 631);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(52, 18);
            this.label10.TabIndex = 25;
            this.label10.Text = "Net Wt";
            this.label10.Visible = false;
            // 
            // txtTotalActual
            // 
            this.txtTotalActual.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotalActual.Location = new System.Drawing.Point(877, 604);
            this.txtTotalActual.Name = "txtTotalActual";
            this.txtTotalActual.Size = new System.Drawing.Size(94, 26);
            this.txtTotalActual.TabIndex = 24;
            this.txtTotalActual.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtTotalActual.Visible = false;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(877, 631);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(68, 18);
            this.label9.TabIndex = 23;
            this.label9.Text = "Actual Wt";
            this.label9.Visible = false;
            // 
            // chckNumpad
            // 
            this.chckNumpad.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.chckNumpad.AutoSize = true;
            this.chckNumpad.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chckNumpad.Location = new System.Drawing.Point(1244, 24);
            this.chckNumpad.Name = "chckNumpad";
            this.chckNumpad.Size = new System.Drawing.Size(95, 27);
            this.chckNumpad.TabIndex = 32;
            this.chckNumpad.Text = "Numpad";
            this.chckNumpad.UseVisualStyleBackColor = true;
            this.chckNumpad.CheckedChanged += new System.EventHandler(this.ChckNumpad_CheckedChanged);
            // 
            // chckbatch
            // 
            this.chckbatch.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.chckbatch.AutoSize = true;
            this.chckbatch.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chckbatch.Location = new System.Drawing.Point(1192, 655);
            this.chckbatch.Name = "chckbatch";
            this.chckbatch.Size = new System.Drawing.Size(143, 27);
            this.chckbatch.TabIndex = 33;
            this.chckbatch.Text = "Batch File Print";
            this.chckbatch.UseVisualStyleBackColor = true;
            // 
            // FrmWarpWeight
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(1347, 694);
            this.Controls.Add(this.chckbatch);
            this.Controls.Add(this.chckNumpad);
            this.Controls.Add(this.txtTotalDiff);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.txttotalTar);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.txtTotalNet);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.txtTotalActual);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.btnBack);
            this.Controls.Add(this.bntPrintBarcode);
            this.Controls.Add(this.grWeight);
            this.Controls.Add(this.txtBeamNo);
            this.Controls.Add(this.txtWeight);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.dtpDate);
            this.Controls.Add(this.btnReset);
            this.Name = "FrmWarpWeight";
            this.Text = "Warp Weight";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.FrmWarpWeight_Load);
            this.grWeight.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DataGridWarpWeight)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox grWeight;
        private System.Windows.Forms.DataGridView DataGridWarpWeight;
        private System.Windows.Forms.Button btnReset;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.TextBox txtBeamNo;
        private System.Windows.Forms.Button btnBack;
        private System.Windows.Forms.Button bntPrintBarcode;
        private System.Windows.Forms.TextBox txtWeight;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtTotalDiff;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txttotalTar;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtTotalNet;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtTotalActual;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.DateTimePicker dtpDate;
        private System.Windows.Forms.CheckBox chckNumpad;
        private TouchScreen touchScreen1;
        private System.Windows.Forms.CheckBox chckbatch;
    }
}