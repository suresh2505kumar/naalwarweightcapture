﻿using System;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace NaalwarWeightCapture
{
    public partial class FrmKPickDataImport : Form
    {
        private BackgroundWorker bw;
        public FrmKPickDataImport()
        {
            InitializeComponent();
            this.bw = new BackgroundWorker();
            this.bw.DoWork += new DoWorkEventHandler(backgroundWorkerExport_DoWork);
            this.bw.ProgressChanged += new ProgressChangedEventHandler(bw_ProgressChanged);
            this.bw.RunWorkerCompleted += new RunWorkerCompletedEventHandler(bw_RunWorkerCompleted);
            this.bw.WorkerReportsProgress = true;
        }

        private void bw_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            lblProcess.Text = "Completed";
            btnImport.Enabled = true;
        }

        private void bw_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            lblProcess.Text = "Processing...";
        }

        SQLDBHelper db = new SQLDBHelper();
        SqlConnection connWeb = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnStrWeb"].ConnectionString);
        private void FrmKPickDataImport_Load(object sender, EventArgs e)
        {
            dtpDate.Format = DateTimePickerFormat.Custom;
            dtpDate.CustomFormat = "dd-MMM-yyyy";
        }

        private void btnImport_Click(object sender, EventArgs e)
        {
            try
            {
                if (!this.bw.IsBusy)
                {
                    this.bw.RunWorkerAsync();
                    this.btnImport.Enabled = false;
                    lblProcess.Text = "Processing...";
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void backgroundWorkerExport_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                int ShiftIdW = 0;
                DateTime dte = Convert.ToDateTime(dtpDate.Text);
                string Month = dte.Month.ToString("00");
                int Year = dte.Year;
                int ShiftId = Convert.ToInt32(cmbShift.Text);
                string day = dte.Day.ToString("00");
                string Query = "select sdate,shift,mcno,picks from PROD_" + Month + Year + " where SDATE = '" + Year + Month + day + "' and shift = " + ShiftId + "";
                DataTable dt = db.GetData(CommandType.Text, Query);
                DataTable dtServer = new DataTable();
                dtServer.Columns.Add("PickUid", typeof(int));
                dtServer.Columns.Add("Dte", typeof(DateTime));
                dtServer.Columns.Add("Shiftname", typeof(string));
                dtServer.Columns.Add("LoomNumber", typeof(string));
                dtServer.Columns.Add("Kpic", typeof(decimal));
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dtServer.NewRow();
                    row["PickUid"] = i + 1;
                    row["Dte"] = Convert.ToDateTime(dt.Rows[i]["sdate"].ToString());
                    if (cmbShift.Text == "1")
                    {
                        ShiftIdW = 984;
                    }
                    else if (cmbShift.Text == "2")
                    {
                        ShiftIdW = 985;
                    }
                    else if (cmbShift.Text == "3")
                    {
                        ShiftIdW = 986;
                    }
                    row["Shiftname"] = ShiftIdW;
                    row["LoomNumber"] = dt.Rows[i]["mcno"].ToString();
                    row["Kpic"] = Convert.ToDecimal(dt.Rows[i]["picks"].ToString());
                    dtServer.Rows.Add(row);
                }
                SqlParameter[] paradelet = {
                    new SqlParameter("@Date",Convert.ToDateTime(dtpDate.Text).ToString("yyyy-MM-dd")),
                    new SqlParameter("@ShifId",ShiftIdW)
                };
                db.ExecuteQuery(CommandType.StoredProcedure, "SP_DeleteImportPickData", paradelet,connWeb);
                using (SqlBulkCopy bulkCopy = new SqlBulkCopy(connWeb))
                {
                    connWeb.Open();
                    bulkCopy.DestinationTableName = "dbo.ImportPickData";
                    bulkCopy.WriteToServer(dtServer);
                    MessageBox.Show("Data Exoprted To Sql Server Succefully", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    connWeb.Close();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                int ShiftIdW = 0;
                int ShiftId = 0;
                DateTime dte = DateTime.Now.Date;
                TimeSpan tme = DateTime.Now.TimeOfDay;
                int t1 = tme.Hours;
                if (t1 >= 8 && t1 < 16)
                {
                    ShiftIdW = 984;
                    ShiftId = 1;
                }
                else if (t1 >= 16 && t1 <= 24)
                {
                    ShiftIdW = 985;
                    ShiftId = 2;
                }
                else if (t1 >= 0 && t1 < 8)
                {
                    ShiftIdW = 986;
                    ShiftId = 3;
                    dte = dte.AddDays(-1);
                }

                string Month = dte.Month.ToString("00");
                int Year = dte.Year;

                string day = dte.Day.ToString("00");
                string Query = "select sdate,shift,mcno,picks from PROD_" + Month + Year + " where SDATE = '" + Year + Month + day + "' and shift = " + ShiftId + "";
                DataTable dt = db.GetData(CommandType.Text, Query);
                DataTable dtServer = new DataTable();
                dtServer.Columns.Add("PickUid", typeof(int));
                dtServer.Columns.Add("Dte", typeof(DateTime));
                dtServer.Columns.Add("Shiftname", typeof(string));
                dtServer.Columns.Add("LoomNumber", typeof(string));
                dtServer.Columns.Add("Kpic", typeof(decimal));
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dtServer.NewRow();
                    row["PickUid"] = i + 1;
                    row["Dte"] = Convert.ToDateTime(dt.Rows[i]["sdate"].ToString());

                    row["Shiftname"] = ShiftIdW;
                    row["LoomNumber"] = dt.Rows[i]["mcno"].ToString();
                    row["Kpic"] = Convert.ToDecimal(dt.Rows[i]["picks"].ToString());
                    dtServer.Rows.Add(row);
                }
                SqlParameter[] paradelet = {
                    new SqlParameter("@Date",Convert.ToDateTime(dtpDate.Text).ToString("yyyy-MM-dd")),
                    new SqlParameter("@ShifId",ShiftIdW)
                };
                db.ExecuteQuery(CommandType.StoredProcedure, "SP_DeleteImportPickData", paradelet, connWeb);
                using (SqlBulkCopy bulkCopy = new SqlBulkCopy(connWeb))
                {
                    connWeb.Open();
                    bulkCopy.DestinationTableName = "dbo.ImportPickData";
                    bulkCopy.WriteToServer(dtServer);
                    // MessageBox.Show("Data Exoprted To Sql Server Succefully", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    connWeb.Close();
                }

            }
            catch (Exception)
            {

            }
        }
    }
}
