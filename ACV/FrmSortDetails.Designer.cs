﻿namespace NaalwarWeightCapture
{
    partial class FrmSortDetails
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.cmbCaptured = new System.Windows.Forms.ComboBox();
            this.btnReset = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.txtRollNo = new System.Windows.Forms.TextBox();
            this.btnBack = new System.Windows.Forms.Button();
            this.bntPrintBarcode = new System.Windows.Forms.Button();
            this.txtWeight = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.grWeight = new System.Windows.Forms.GroupBox();
            this.DataGridSortWeight = new System.Windows.Forms.DataGridView();
            this.dtpDate = new System.Windows.Forms.DateTimePicker();
            this.chckNumpad = new System.Windows.Forms.CheckBox();
            this.chckbatch = new System.Windows.Forms.CheckBox();
            this.touchScreen1 = new NaalwarWeightCapture.TouchScreen();
            this.grWeight.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridSortWeight)).BeginInit();
            this.SuspendLayout();
            // 
            // cmbCaptured
            // 
            this.cmbCaptured.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.cmbCaptured.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbCaptured.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbCaptured.FormattingEnabled = true;
            this.cmbCaptured.Items.AddRange(new object[] {
            "Doffed",
            "Loaded",
            "Weighed"});
            this.cmbCaptured.Location = new System.Drawing.Point(1063, 122);
            this.cmbCaptured.Name = "cmbCaptured";
            this.cmbCaptured.Size = new System.Drawing.Size(188, 31);
            this.cmbCaptured.TabIndex = 2;
            this.cmbCaptured.SelectedIndexChanged += new System.EventHandler(this.CmbCaptured_SelectedIndexChanged);
            // 
            // btnReset
            // 
            this.btnReset.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnReset.BackColor = System.Drawing.Color.Yellow;
            this.btnReset.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReset.Location = new System.Drawing.Point(1065, 438);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(188, 85);
            this.btnReset.TabIndex = 6;
            this.btnReset.Text = "Refresh";
            this.btnReset.UseVisualStyleBackColor = false;
            this.btnReset.Click += new System.EventHandler(this.BtnReset_Click);
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.BackColor = System.Drawing.Color.Lime;
            this.btnSave.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.Location = new System.Drawing.Point(1065, 350);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(188, 85);
            this.btnSave.TabIndex = 5;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.BtnSave_Click);
            // 
            // txtRollNo
            // 
            this.txtRollNo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtRollNo.Font = new System.Drawing.Font("Calibri", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRollNo.Location = new System.Drawing.Point(1062, 179);
            this.txtRollNo.Multiline = true;
            this.txtRollNo.Name = "txtRollNo";
            this.txtRollNo.Size = new System.Drawing.Size(193, 67);
            this.txtRollNo.TabIndex = 3;
            this.txtRollNo.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtRollNo.Enter += new System.EventHandler(this.TxtRollNo_Enter);
            this.txtRollNo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TxtRollNo_KeyDown);
            // 
            // btnBack
            // 
            this.btnBack.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnBack.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.btnBack.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBack.Location = new System.Drawing.Point(1065, 615);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(188, 85);
            this.btnBack.TabIndex = 8;
            this.btnBack.Text = "Exit";
            this.btnBack.UseVisualStyleBackColor = false;
            this.btnBack.Click += new System.EventHandler(this.BtnBack_Click);
            // 
            // bntPrintBarcode
            // 
            this.bntPrintBarcode.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.bntPrintBarcode.BackColor = System.Drawing.Color.SpringGreen;
            this.bntPrintBarcode.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bntPrintBarcode.Location = new System.Drawing.Point(1065, 526);
            this.bntPrintBarcode.Name = "bntPrintBarcode";
            this.bntPrintBarcode.Size = new System.Drawing.Size(188, 85);
            this.bntPrintBarcode.TabIndex = 7;
            this.bntPrintBarcode.Text = "Print Barcode";
            this.bntPrintBarcode.UseVisualStyleBackColor = false;
            this.bntPrintBarcode.Click += new System.EventHandler(this.BntPrintBarcode_Click);
            // 
            // txtWeight
            // 
            this.txtWeight.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtWeight.Font = new System.Drawing.Font("Calibri", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtWeight.Location = new System.Drawing.Point(1065, 280);
            this.txtWeight.Name = "txtWeight";
            this.txtWeight.Size = new System.Drawing.Size(188, 66);
            this.txtWeight.TabIndex = 4;
            this.txtWeight.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtWeight.Enter += new System.EventHandler(this.TxtWeight_Enter);
            this.txtWeight.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TxtWeight_KeyDown);
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Calibri", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(1127, 239);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(93, 33);
            this.label6.TabIndex = 19;
            this.label6.Text = "Weight";
            // 
            // label5
            // 
            this.label5.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Calibri", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(1122, 155);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(92, 33);
            this.label5.TabIndex = 18;
            this.label5.Text = "Roll No";
            // 
            // grWeight
            // 
            this.grWeight.AutoSize = true;
            this.grWeight.Controls.Add(this.touchScreen1);
            this.grWeight.Controls.Add(this.DataGridSortWeight);
            this.grWeight.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grWeight.Location = new System.Drawing.Point(2, 3);
            this.grWeight.Name = "grWeight";
            this.grWeight.Size = new System.Drawing.Size(1159, 533);
            this.grWeight.TabIndex = 1;
            this.grWeight.TabStop = false;
            // 
            // DataGridSortWeight
            // 
            this.DataGridSortWeight.AllowUserToAddRows = false;
            this.DataGridSortWeight.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DataGridSortWeight.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.DataGridSortWeight.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridSortWeight.Location = new System.Drawing.Point(14, 19);
            this.DataGridSortWeight.Name = "DataGridSortWeight";
            this.DataGridSortWeight.ReadOnly = true;
            this.DataGridSortWeight.RowHeadersVisible = false;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DataGridSortWeight.RowsDefaultCellStyle = dataGridViewCellStyle2;
            this.DataGridSortWeight.RowTemplate.Height = 30;
            this.DataGridSortWeight.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DataGridSortWeight.Size = new System.Drawing.Size(1081, 487);
            this.DataGridSortWeight.TabIndex = 8;
            this.DataGridSortWeight.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.DataGridSortWeight_CellFormatting);
            this.DataGridSortWeight.MouseClick += new System.Windows.Forms.MouseEventHandler(this.DataGridSortWeight_MouseClick);
            // 
            // dtpDate
            // 
            this.dtpDate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.dtpDate.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpDate.Location = new System.Drawing.Point(1115, 63);
            this.dtpDate.Name = "dtpDate";
            this.dtpDate.Size = new System.Drawing.Size(136, 31);
            this.dtpDate.TabIndex = 1;
            this.dtpDate.ValueChanged += new System.EventHandler(this.DtpDate_ValueChanged);
            // 
            // chckNumpad
            // 
            this.chckNumpad.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.chckNumpad.AutoSize = true;
            this.chckNumpad.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chckNumpad.Location = new System.Drawing.Point(1156, 32);
            this.chckNumpad.Name = "chckNumpad";
            this.chckNumpad.Size = new System.Drawing.Size(95, 27);
            this.chckNumpad.TabIndex = 0;
            this.chckNumpad.Text = "Numpad";
            this.chckNumpad.UseVisualStyleBackColor = true;
            this.chckNumpad.CheckedChanged += new System.EventHandler(this.ChckNumpad_CheckedChanged);
            // 
            // chckbatch
            // 
            this.chckbatch.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.chckbatch.AutoSize = true;
            this.chckbatch.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chckbatch.Location = new System.Drawing.Point(1103, 3);
            this.chckbatch.Name = "chckbatch";
            this.chckbatch.Size = new System.Drawing.Size(143, 27);
            this.chckbatch.TabIndex = 20;
            this.chckbatch.Text = "Batch File Print";
            this.chckbatch.UseVisualStyleBackColor = true;
            // 
            // touchScreen1
            // 
            this.touchScreen1.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.touchScreen1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.touchScreen1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.touchScreen1.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.touchScreen1.Location = new System.Drawing.Point(930, 19);
            this.touchScreen1.Margin = new System.Windows.Forms.Padding(4);
            this.touchScreen1.Name = "touchScreen1";
            this.touchScreen1.Size = new System.Drawing.Size(222, 389);
            this.touchScreen1.TabIndex = 9;
            this.touchScreen1.Visible = false;
            // 
            // FrmSortDetails
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(1263, 740);
            this.Controls.Add(this.chckNumpad);
            this.Controls.Add(this.btnBack);
            this.Controls.Add(this.grWeight);
            this.Controls.Add(this.cmbCaptured);
            this.Controls.Add(this.txtRollNo);
            this.Controls.Add(this.txtWeight);
            this.Controls.Add(this.dtpDate);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.chckbatch);
            this.Controls.Add(this.btnReset);
            this.Controls.Add(this.bntPrintBarcode);
            this.Name = "FrmSortDetails";
            this.Text = "Sort Details Version 1.1";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.FrmSortDetails_Load);
            this.Enter += new System.EventHandler(this.FrmSortDetails_Enter);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FrmSortDetails_KeyDown);
            this.grWeight.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DataGridSortWeight)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button btnReset;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.TextBox txtRollNo;
        private System.Windows.Forms.Button btnBack;
        private System.Windows.Forms.Button bntPrintBarcode;
        private System.Windows.Forms.TextBox txtWeight;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox cmbCaptured;
        private System.Windows.Forms.GroupBox grWeight;
        private System.Windows.Forms.DataGridView DataGridSortWeight;
        private System.Windows.Forms.DateTimePicker dtpDate;
        private System.Windows.Forms.CheckBox chckNumpad;
        private TouchScreen touchScreen1;
        private System.Windows.Forms.CheckBox chckbatch;
    }
}