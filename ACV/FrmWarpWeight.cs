﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Printing;
using System.IO;
using System.Windows.Forms;

namespace NaalwarWeightCapture
{
    public partial class FrmWarpWeight : Form
    {
        private TextBox focusedTextbox = null;
        public FrmWarpWeight()
        {
            InitializeComponent();
            this.FormClosing += FrmWarpWeight_FormClosing;
            touchScreen1.OnUserControlButtonClicked += new TouchScreen.ButtonClickedEventHandler(TouchScreen1_OnUserControlButtonClicked);
        }

        private void TouchScreen1_OnUserControlButtonClicked(object sender, EventArgs e)
        {
            try
            {
                Button b = (Button)sender;
                if (focusedTextbox != null)
                {
                    if (b.Text == "Back")
                    {
                        if (focusedTextbox.Text.Length > 1)
                        {
                            focusedTextbox.Text = focusedTextbox.Text.Substring(0, focusedTextbox.Text.Length - 1);
                        }
                        else
                        {
                            focusedTextbox.Text = string.Empty;
                        }
                    }
                    else if (b.Text == "Clear")
                    {
                        focusedTextbox.Text = string.Empty;
                        focusedTextbox.Focus();
                    }
                    else if (b.Text == "TAB")
                    {
                        this.SelectNextControl(focusedTextbox, true, true, true, true);
                    }
                    else if (b.Text == "Enter")
                    {
                    }
                    else
                    {
                        if (MyGlobal.bTouch)
                            focusedTextbox.Text = b.Text;
                        else
                        {
                            MyGlobal.bTouch = false;
                            focusedTextbox.Text += b.Text;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Infromation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void FrmWarpWeight_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                if (e.CloseReason == CloseReason.UserClosing)
                {
                    DialogResult result = MessageBox.Show("Do you really want to go back ?", "Closing", MessageBoxButtons.YesNo, MessageBoxIcon.Stop);
                    if (result == DialogResult.Yes)
                    {
                        this.Hide();
                        FrmIOSource io = new FrmIOSource();
                        io.Show();
                    }
                    else
                    {
                        e.Cancel = true;
                    }
                }
                else
                {
                    e.Cancel = true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Infromation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnStr"].ConnectionString);
        SQLDBHelper db = new SQLDBHelper();
        private void FrmWarpWeight_Load(object sender, EventArgs e)
        {
            DataGridWarpWeight.EnableHeadersVisualStyles = false;
            DataGridWarpWeight.ColumnHeadersDefaultCellStyle.BackColor = Color.LightSeaGreen;
            grWeight.Height = this.Height - 100;
            DataGridWarpWeight.Height = this.Height - 100;
            grWeight.Width = this.Width - 250;
            DataGridWarpWeight.Width = this.Width - 250;
            GetWarbBooking();
            chckbatch.Checked = true;
        }
        private void GetWarbBooking()
        {
            try
            {
                SqlParameter[] para = { new SqlParameter("@Tag", "Select"), new SqlParameter("Date", Convert.ToDateTime(dtpDate.Text)) };
                DataTable dt = db.GetData(CommandType.StoredProcedure, "SP_GetWarbBooking", para);
                LoadTitle(dt);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void LoadTitle(DataTable dt)
        {
            DataGridWarpWeight.DataSource = null;
            DataGridWarpWeight.AutoGenerateColumns = false;
            DataGridWarpWeight.ColumnCount = 14;
            DataGridWarpWeight.Columns[0].Name = "WBUid";
            DataGridWarpWeight.Columns[0].HeaderText = "WBUid";
            DataGridWarpWeight.Columns[0].Visible = false;

            DataGridWarpWeight.Columns[1].Name = "DocDate";
            DataGridWarpWeight.Columns[1].HeaderText = "DocDate";
            DataGridWarpWeight.Columns[1].Width = 110;

            DataGridWarpWeight.Columns[2].Name = "Location";
            DataGridWarpWeight.Columns[2].HeaderText = "Location";

            DataGridWarpWeight.Columns[3].Name = "ItemName";
            DataGridWarpWeight.Columns[3].HeaderText = "ItemName";
            DataGridWarpWeight.Columns[3].Width = 110;

            DataGridWarpWeight.Columns[4].Name = "Beam";
            DataGridWarpWeight.Columns[4].HeaderText = "Beam";
            DataGridWarpWeight.Columns[4].Width = 80;

            DataGridWarpWeight.Columns[5].Name = "SetNo";
            DataGridWarpWeight.Columns[5].HeaderText = "SetNo";
            DataGridWarpWeight.Columns[5].Width = 70;

            DataGridWarpWeight.Columns[6].Name = "Ends";
            DataGridWarpWeight.Columns[6].HeaderText = "Ends";
            DataGridWarpWeight.Columns[6].Width = 100;
            
            DataGridWarpWeight.Columns[7].Name = "WBLength";
            DataGridWarpWeight.Columns[7].HeaderText = "WBLength";

            DataGridWarpWeight.Columns[8].Name = "Gross";
            DataGridWarpWeight.Columns[8].HeaderText = "Gross";
            DataGridWarpWeight.Columns[8].Width = 90;


            DataGridWarpWeight.Columns[9].Name = "TareWight";
            DataGridWarpWeight.Columns[9].HeaderText = "TareWight";
            DataGridWarpWeight.Columns[9].Width = 80;

            DataGridWarpWeight.Columns[10].Name = "Net";
            DataGridWarpWeight.Columns[10].HeaderText = "Net";
            DataGridWarpWeight.Columns[10].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            DataGridWarpWeight.Columns[10].DefaultCellStyle.Format = "N3";
            DataGridWarpWeight.Columns[10].Width = 90;
            DataGridWarpWeight.Columns[11].Name = "Actual";
            DataGridWarpWeight.Columns[11].HeaderText = "ActualWght";
            DataGridWarpWeight.Columns[11].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            DataGridWarpWeight.Columns[11].DefaultCellStyle.Format = "N3";
            DataGridWarpWeight.Columns[11].Width = 90;
            DataGridWarpWeight.Columns[12].Name = "Diffrence";
            DataGridWarpWeight.Columns[12].HeaderText = "Diffrence";
            DataGridWarpWeight.Columns[12].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            DataGridWarpWeight.Columns[12].DefaultCellStyle.Format = "N3";
            DataGridWarpWeight.Columns[12].Width = 90;
            DataGridWarpWeight.Columns[13].Name = "Barcode";
            DataGridWarpWeight.Columns[13].HeaderText = "Barcode";
            DataGridWarpWeight.Columns[13].Visible = false;

            if (dt.Rows.Count != 0)
            {
                DataGridWarpWeight.Columns[0].DataPropertyName = "WBUid";
                DataGridWarpWeight.Columns[1].DataPropertyName = "DocDate";
                DataGridWarpWeight.Columns[2].DataPropertyName = "Location";
                DataGridWarpWeight.Columns[3].DataPropertyName = "ItemName";
                DataGridWarpWeight.Columns[4].DataPropertyName = "Beam";
                DataGridWarpWeight.Columns[5].DataPropertyName = "SetNo";
                DataGridWarpWeight.Columns[6].DataPropertyName = "Ends";
                DataGridWarpWeight.Columns[7].DataPropertyName = "WBLength";
                DataGridWarpWeight.Columns[8].DataPropertyName = "WBWeight";
                DataGridWarpWeight.Columns[9].DataPropertyName = "BeamWeight";
                DataGridWarpWeight.Columns[10].DataPropertyName = "Net";
                DataGridWarpWeight.Columns[11].DataPropertyName = "ActualWght";
                DataGridWarpWeight.Columns[12].DataPropertyName = "Diff";
                DataGridWarpWeight.Columns[13].DataPropertyName = "Barcode";
                DataGridWarpWeight.DataSource = dt;
            }
        }

        private void DataGridWarpWeight_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                int index = DataGridWarpWeight.SelectedCells[0].RowIndex;
                txtBeamNo.Text = DataGridWarpWeight.Rows[index].Cells[4].Value.ToString();
                txtWeight.Text = string.Empty;
                txtWeight.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Infromation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void BtnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (DataGridWarpWeight.SelectedRows.Count != 0)
                {
                    int Index = DataGridWarpWeight.SelectedCells[0].RowIndex;
                    int JorListUid = Convert.ToInt32(DataGridWarpWeight.Rows[Index].Cells[0].Value.ToString());
                    decimal ActualWait = Convert.ToDecimal(txtWeight.Text);
                    SqlParameter[] para = {
                          new SqlParameter("@WBUid",JorListUid),
                          new SqlParameter("@WBWeight",ActualWait)
                     };
                    int i = db.ExecuteQuery(CommandType.StoredProcedure, "SP_UpdateWarbBooking", para);
                    decimal Tare = Convert.ToDecimal(DataGridWarpWeight.Rows[Index].Cells[9].Value);
                    DataGridWarpWeight.Rows[Index].Cells[11].Value = (ActualWait).ToString("0.0000");
                    txtWeight.Text = string.Empty;
                    txtBeamNo.Text = string.Empty;
                    BntPrintBarcode_Click(sender, e);
                    GetWarbBooking();
                }
                else
                {
                    MessageBox.Show("Select Beam  Number ", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void BntPrintBarcode_Click(object sender, EventArgs e)
        {
            try
            {
                if (chckbatch.Checked == true)
                {
                    if (DataGridWarpWeight.SelectedRows.Count != 0)
                    {
                        DataTable dt = db.GetData(CommandType.StoredProcedure, "SP_PrintBarcode");
                        FileStream fs = new FileStream(Application.StartupPath + "\\Barcode01.txt", FileMode.Truncate, FileAccess.Write);
                        fs.Close();
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            string Var = dt.Rows[i]["VarType"].ToString();
                            if (Var == "Static")
                            {
                                File.AppendAllText(Application.StartupPath + "\\Barcode01.txt", dt.Rows[i]["BCodeFormat"].ToString() + Environment.NewLine);
                            }
                            else
                            {
                                int LineNUmber = Convert.ToInt32(dt.Rows[i]["LineNumber"].ToString());
                                int Index = DataGridWarpWeight.SelectedCells[0].RowIndex;
                                if (LineNUmber == 35)
                                {
                                    string text = dt.Rows[i]["BCodeFormat"].ToString() + "\"" + DataGridWarpWeight.Rows[Index].Cells[2].Value.ToString() + "\"";
                                    File.AppendAllText(Application.StartupPath + "\\Barcode01.txt", text + Environment.NewLine);
                                }
                                else if (LineNUmber == 36)
                                {
                                    string text = dt.Rows[i]["BCodeFormat"].ToString() + "\"" + DataGridWarpWeight.Rows[Index].Cells[4].Value.ToString() + "\"";
                                    File.AppendAllText(Application.StartupPath + "\\Barcode01.txt", text + Environment.NewLine);
                                }
                                else if (LineNUmber == 37)
                                {
                                    string text = dt.Rows[i]["BCodeFormat"].ToString() + "\"" + DataGridWarpWeight.Rows[Index].Cells[3].Value.ToString() + "\"";
                                    File.AppendAllText(Application.StartupPath + "\\Barcode01.txt", text + Environment.NewLine);
                                }
                                else if (LineNUmber == 38)
                                {
                                    string text = dt.Rows[i]["BCodeFormat"].ToString() + "\"" + Convert.ToDecimal(DataGridWarpWeight.Rows[Index].Cells[6].Value.ToString()).ToString("0") + "\"";
                                    File.AppendAllText(Application.StartupPath + "\\Barcode01.txt", text + Environment.NewLine);
                                }
                                else if (LineNUmber == 39)
                                {
                                    string text = dt.Rows[i]["BCodeFormat"].ToString() + "\"" + Convert.ToDecimal(DataGridWarpWeight.Rows[Index].Cells[7].Value.ToString()).ToString("0") + "\"";
                                    File.AppendAllText(Application.StartupPath + "\\Barcode01.txt", text + Environment.NewLine);
                                }
                                else if (LineNUmber == 40)
                                {
                                    string text = dt.Rows[i]["BCodeFormat"].ToString() + "\"" + DataGridWarpWeight.Rows[Index].Cells[5].Value.ToString() + "\"";
                                    File.AppendAllText(Application.StartupPath + "\\Barcode01.txt", text + Environment.NewLine);
                                }
                                else if (LineNUmber == 41)
                                {
                                    string text = dt.Rows[i]["BCodeFormat"].ToString() + "\"" + Convert.ToDateTime(DataGridWarpWeight.Rows[Index].Cells[1].Value.ToString()).ToString("dd.MM.yyyy") + "\"";
                                    File.AppendAllText(Application.StartupPath + "\\Barcode01.txt", text + Environment.NewLine);
                                }
                                else if (LineNUmber == 42)
                                {
                                    string text = dt.Rows[i]["BCodeFormat"].ToString() + "\"" + Convert.ToDecimal(DataGridWarpWeight.Rows[Index].Cells[8].Value.ToString()).ToString("0.000") + "\"";
                                    File.AppendAllText(Application.StartupPath + "\\Barcode01.txt", text + Environment.NewLine);
                                }
                                else if (LineNUmber == 43)
                                {
                                    string text = dt.Rows[i]["BCodeFormat"].ToString() + "\"" + Convert.ToDecimal(DataGridWarpWeight.Rows[Index].Cells[9].Value.ToString()).ToString("0.000") + "\"";
                                    File.AppendAllText(Application.StartupPath + "\\Barcode01.txt", text + Environment.NewLine);
                                }
                                else if (LineNUmber == 44)
                                {
                                    decimal Tare = Convert.ToDecimal(DataGridWarpWeight.Rows[Index].Cells[9].Value.ToString());
                                    decimal Weg = Convert.ToDecimal(DataGridWarpWeight.Rows[Index].Cells[11].Value.ToString());
                                    string text = dt.Rows[i]["BCodeFormat"].ToString() + "\"" + (Weg - Tare).ToString("0.000") + "\"";
                                    File.AppendAllText(Application.StartupPath + "\\Barcode01.txt", text + Environment.NewLine);
                                }
                                else if (LineNUmber == 45)
                                {
                                    string text = dt.Rows[i]["BCodeFormat"].ToString() + "\"" + DataGridWarpWeight.Rows[Index].Cells[13].Value.ToString() + "\"";
                                    File.AppendAllText(Application.StartupPath + "\\Barcode01.txt", text + Environment.NewLine);
                                }
                                else if (LineNUmber == 46)
                                {
                                    string text = dt.Rows[i]["BCodeFormat"].ToString() + "\"" + DataGridWarpWeight.Rows[Index].Cells[13].Value.ToString() + "\"";
                                    File.AppendAllText(Application.StartupPath + "\\Barcode01.txt", text + Environment.NewLine);
                                }
                            }
                        }
                        int j = 2;
                        for (int i = 0; i < j; i++)
                        {
                            Process.Start(Application.StartupPath + "\\BPBeam.bat");
                        }
                    }
                }
                else
                {
                    if (DataGridWarpWeight.SelectedRows.Count != 0)
                    {
                        int Index = DataGridWarpWeight.SelectedCells[0].RowIndex;
                        //Generate Barcode
                        KeepDynamic.Barcode.CrystalReport.BarCode br = new KeepDynamic.Barcode.CrystalReport.BarCode();
                        br.SymbologyType = KeepDynamic.Barcode.CrystalReport.SymbologyType.Code128;
                        br.CodeText = DataGridWarpWeight.Rows[Index].Cells[9].Value.ToString();
                        br.DisplayCodeText = false;
                        byte[] bt = br.drawBarcodeAsBytes();
                        //Net Weight
                        decimal Tare = Convert.ToDecimal(DataGridWarpWeight.Rows[Index].Cells[10].Value.ToString());
                        decimal Weg = Convert.ToDecimal(DataGridWarpWeight.Rows[Index].Cells[7].Value.ToString());
                        DateTime dtgrn = Convert.ToDateTime(DataGridWarpWeight.Rows[Index].Cells[1].Value.ToString());

                        DataTable dt = new DataTable();
                        dt.Columns.Add("Barcode", typeof(byte[]));
                        dt.Columns.Add("Sizer", typeof(string));
                        dt.Columns.Add("BeamNo", typeof(string));
                        dt.Columns.Add("Count", typeof(string));
                        dt.Columns.Add("Ends", typeof(string));
                        dt.Columns.Add("Mtrs", typeof(string));
                        dt.Columns.Add("SetNo", typeof(string));
                        dt.Columns.Add("GWt", typeof(decimal));
                        dt.Columns.Add("TWt", typeof(decimal));
                        dt.Columns.Add("NWt", typeof(decimal));
                        dt.Columns.Add("Date", typeof(DateTime));
                        dt.Columns.Add("BarText", typeof(string));
                        DataRow row = dt.NewRow();
                        row["Barcode"] = bt;
                        row["Sizer"] = DataGridWarpWeight.Rows[Index].Cells[8].Value.ToString();
                        row["BeamNo"] = DataGridWarpWeight.Rows[Index].Cells[3].Value.ToString();
                        row["Count"] = DataGridWarpWeight.Rows[Index].Cells[2].Value.ToString();
                        row["Ends"] = Convert.ToDecimal(DataGridWarpWeight.Rows[Index].Cells[5].Value.ToString()).ToString("0");
                        row["Mtrs"] = Convert.ToDecimal(DataGridWarpWeight.Rows[Index].Cells[6].Value.ToString()).ToString("0");
                        row["SetNo"] = Convert.ToDecimal(DataGridWarpWeight.Rows[Index].Cells[4].Value.ToString()).ToString("0");
                        row["GWt"] = Convert.ToDecimal(DataGridWarpWeight.Rows[Index].Cells[7].Value.ToString()).ToString("0.000");
                        row["TWt"] = Convert.ToDecimal(DataGridWarpWeight.Rows[Index].Cells[10].Value.ToString()).ToString("0.000");
                        row["NWt"] = (Weg - Tare).ToString("0.000");
                        row["Date"] = dtgrn;
                        row["BarText"] = DataGridWarpWeight.Rows[Index].Cells[9].Value.ToString();
                        dt.Rows.Add(row);
                        ReportDocument doc = new ReportDocument();
                        doc.Load(Application.StartupPath + "\\CryBeamBarcode.rpt");
                        doc.SetDataSource(dt);
                        PrintDocument pDoc = new PrintDocument();
                        PrintLayoutSettings PrintLayout = new PrintLayoutSettings();
                        PrinterSettings printerSettings = new PrinterSettings();
                        printerSettings.PrinterName = pDoc.PrinterSettings.PrinterName;
                        PageSettings pSettings = new PageSettings(printerSettings);
                        pSettings.PrinterSettings.Copies = 1;
                        pSettings.Margins.Left = 1;
                        doc.PrintOptions.DissociatePageSizeAndPrinterPaperSize = true;
                        doc.PrintOptions.PrinterDuplex = PrinterDuplex.Simplex;
                        doc.PrintToPrinter(printerSettings, pSettings, false, PrintLayout);
                    }
                    else
                    {
                        MessageBox.Show("Select Beam Number to Print Barcode ", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Infromation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

        }

        private void BtnReset_Click(object sender, EventArgs e)
        {
            ClearControl();
            GetWarbBooking();
            txtWeight.Focus();
        }

        private void ClearControl()
        {
            txtBeamNo.Text = string.Empty;
            txtWeight.Text = string.Empty;
        }

        private void BtnBack_Click(object sender, EventArgs e)
        {
            this.Hide();
            FrmIOSource grn = new FrmIOSource();
            grn.Show();
        }

        private void txtWeight_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    BtnSave_Click(sender, e);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information); ;
                return;
            }
        }

        private void DataGridWarpWeight_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            try
            {

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information); ;
                return;
            }
        }

        private void DtpDate_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                SqlParameter[] para = { new SqlParameter("@Tag", "Select"), new SqlParameter("Date", Convert.ToDateTime(dtpDate.Text)) };
                DataTable dt = db.GetData(CommandType.StoredProcedure, "SP_GetWarbBooking", para);
                LoadTitle(dt);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information); ;
                return;
            }
        }

        private void ChckNumpad_CheckedChanged(object sender, EventArgs e)
        {
            if (chckNumpad.Checked == true)
            {
                touchScreen1.Visible = true;
                txtWeight.Focus();
            }
            else
            {
                touchScreen1.Visible = false;
            }
        }

        private void TxtWeight_Enter(object sender, EventArgs e)
        {
            focusedTextbox = (TextBox)sender;
        }
    }
}
