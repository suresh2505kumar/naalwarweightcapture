﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Configuration;

namespace NaalwarWeightCapture
{
    public partial class FrmyarnReceipt : Form
    {
        public FrmyarnReceipt()
        {
            this.BackColor = Color.White; ;
            InitializeComponent();
            txtBeam.GotFocus += TxtBeam_GotFocus;
            txtItem.GotFocus += txtitem_Click;
            this.FormClosing += FrmyarnReceipt_FormClosing;
        }

        private void FrmyarnReceipt_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                this.Hide();
                FrmIOSource io = new FrmIOSource();
                io.Show();
            }
            else
            {
                e.Cancel = true;
            }
        }

        private void TxtBeam_GotFocus(object sender, EventArgs e)
        {
            try
            {
                txtdcqty_Click(sender, e);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        int BUid = 0;
        string uid = "";
        public int mode = 0;
        SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnStr"].ConnectionString);
        SQLDBHelper db = new SQLDBHelper();
        SqlCommand qur = new SqlCommand();
        BindingSource bsParty = new BindingSource();
        BindingSource bsItem = new BindingSource();
        BindingSource bsBeam = new BindingSource();
        BindingSource bssearch = new BindingSource();
        BindingSource bsMill = new BindingSource();
        int Fillid = 0;
        public int SelectId = 0;
        int YearId, MonthId, LastNo;
        int EditMode = 0;
        private void txtgrn_TextChanged(object sender, EventArgs e)
        {

        }

        private void dtpgrndt_ValueChanged(object sender, EventArgs e)
        {

        }

        private void Editpnl_Paint(object sender, PaintEventArgs e)
        {

        }

        private void FrmyarnIssue_Load(object sender, EventArgs e)
        {
            qur.Connection = conn;
            string da = DateTime.Today.ToString("MMM/yyyy");
            dtpfnt.Text = da;
            //Left = (MdiParent.ClientRectangle.Width - Width) / 3;
            //Top = (MdiParent.ClientRectangle.Height - Height) / 3;
            Genclass.Module.buttonstyleform(this);
            Genclass.Module.buttonstylepanel(panadd);
            panadd.Visible = true;
            Genpan.Visible = true;
            Editpnl.Visible = false;
            chkact.Checked = true;
            DataGridReceipt.RowHeadersVisible = false;
            Titlep();
            dtpfnt_ValueChanged(sender, e);
            btnsave.Text = "Save";
            DataGridReceipt.AllowUserToDeleteRows = true;
        }

        private void buttnext1_Click(object sender, EventArgs e)
        {
            this.Close();
            FrmIOSource io = new FrmIOSource();
            io.Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            mode = 1;
            Genpan.Visible = false;
            panadd.Visible = false;
            Genclass.Module.ClearTextBox(this, Editpnl);
            Genclass.STR = "";
            txtbar.Text = Genclass.Str5;
            SqlParameter[] para = { new SqlParameter("@BarType", "YarnReceipt") };
            DataTable dt = db.GetData(CommandType.StoredProcedure, "SP_BarcodePrint", para);
            YearId = Convert.ToInt32(dt.Rows[0]["YerarId"].ToString());
            MonthId = Convert.ToInt32(dt.Rows[0]["MonthId"].ToString());
            LastNo = Convert.ToInt32(dt.Rows[0]["LastSNo"].ToString());
            BUid = Convert.ToInt32(dt.Rows[0]["Uid"].ToString());
            Editpnl.Visible = true;

            Genclass.sum1 = 0;
            Genclass.sum2 = 0;
            Genclass.sum3 = 0;
            Genclass.sum4 = 0;
            Genclass.sum5 = 0;
            DataGridReceipt.Rows.Clear();
            txttotal.Text = string.Empty;
            txttwt.Text = string.Empty;
            txtnetwt.Text = string.Empty;
            txtactualwt.Text = string.Empty;
            txtdifwt.Text = string.Empty;
        }
        public void ClearControl()
        {

        }

        private void btnaddrcan_Click(object sender, EventArgs e)
        {
            Editpnl.Visible = false;
            Genpan.Visible = true;
            panadd.Visible = true;
            btnsave.Text = "Save";
            Genclass.Module.ClearTextBox(this, Editpnl);
            DataGridReceipt.Rows.Clear();
        }
        private void button9_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void label10_Click(object sender, EventArgs e)
        {

        }

        private void txtdcqty_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectId == 0)
                {
                    bsBeam.Filter = string.Format("GeneralName LIKE '%{0}%' ", txtBeam.Text);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void label9_Click(object sender, EventArgs e)
        {

        }

        private void txtqty_TextChanged(object sender, EventArgs e)
        {

        }

        protected bool CheckGridValue()
        {
            bool entryFound = false;
            try
            {
                foreach (DataGridViewRow row in DataGridReceipt.Rows)
                {
                    object val2 = row.Cells[3].Value;
                    if (val2 != null && val2.ToString() == txtBeam.Text)
                    {
                        entryFound = true;
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            return entryFound;
        }

        private void btnadd_Click(object sender, EventArgs e)
        {
            if(txtItem.Text != string.Empty && txtlength.Text != string.Empty && txtqty.Text != string.Empty)
            {
                MessageBox.Show("Item and Gross weight should't empty", "Inormation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            bool Beam = CheckGridValue();
            if (Beam == false)
            {
                DataGridReceipt.AutoGenerateColumns = false;
                var index = DataGridReceipt.Rows.Add();
                DataGridReceipt.Rows[index].Cells[0].Value = txtItem.Text;
                DataGridReceipt.Rows[index].Cells[1].Value = txtsetno.Text;
                DataGridReceipt.Rows[index].Cells[2].Value = txtends.Text;
                DataGridReceipt.Rows[index].Cells[3].Value = txtBeam.Text;

                double sump = Convert.ToDouble(txtlength.Text);
                DataGridReceipt.Rows[index].Cells[4].Value = sump.ToString("0.000");

                double sump2 = Convert.ToDouble(txtqty.Text);
                DataGridReceipt.Rows[index].Cells[5].Value = sump2.ToString("0.000");

                double sump3 = Convert.ToDouble(txtbeamwt.Text);
                DataGridReceipt.Rows[index].Cells[6].Value = sump3.ToString("0.000");
                double yui = Convert.ToDouble(txtqty.Text) - Convert.ToDouble(txtbeamwt.Text);
                DataGridReceipt.Rows[index].Cells[7].Value = yui.ToString("0.000");

                DataGridReceipt.Rows[index].Cells[8].Value = "0.000";
                DataGridReceipt.Rows[index].Cells[9].Value = txtItem.Tag;
                DataGridReceipt.Rows[index].Cells[10].Value = 0;
                DataGridReceipt.Rows[index].Cells[11].Value = txtBeam.Tag;
                if(EditMode == 1)
                {
                    DataGridReceipt.Rows[index].Cells[12].Value = txtvehicle.Tag;
                    EditMode = 0;
                }
                else
                {
                    LastNo += 1;
                    string yu = LastNo.ToString("D3");
                    DataGridReceipt.Rows[index].Cells[12].Value = YearId.ToString() + MonthId.ToString("D2") + BUid + yu;
                }
                double yu2 = Convert.ToDouble(txtbeamwt.Text) - Convert.ToDouble(yui);
                DataGridReceipt.Rows[index].Cells[13].Value = yu2.ToString("0.000");
                Genclass.sum1 = Convert.ToDouble(DataGridReceipt.Rows[index].Cells[5].Value);
                txttotal.Text = Genclass.sum1.ToString("0.000");
                Genclass.sum2 = Convert.ToDouble(DataGridReceipt.Rows[index].Cells[6].Value);
                txttwt.Text = Genclass.sum2.ToString("0.000");
                Genclass.sum3 = Convert.ToDouble(DataGridReceipt.Rows[index].Cells[7].Value);
                txtnetwt.Text = Genclass.sum3.ToString("0.000");
                Genclass.sum4 = Convert.ToDouble(DataGridReceipt.Rows[index].Cells[8].Value);
                txtactualwt.Text = Genclass.sum4.ToString("0.000");
                Genclass.sum5 = Convert.ToDouble(DataGridReceipt.Rows[index].Cells[13].Value);
                txtdifwt.Text = Genclass.sum5.ToString("0.000");
                DataGridReceipt.Rows[index].Cells[14].Value = txtMillName.Text;
                DataGridReceipt.Rows[index].Cells[15].Value = txtMillName.Tag;
                txtItem.Text = "";
                txtBeam.Text = "";
                txtqty.Text = "";
                txtends.Text = "";
                txtlength.Text = "";
                txtbeamwt.Text = "";
                txtsetno.Text = "";
            }
            else
            {
                MessageBox.Show("Beam No Already exists", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtBeam.Focus();
                return;
            }

        }

        private void txtname_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectId == 0)
                {
                    bsParty.Filter = string.Format("Name LIKE '%{0}%' ", txtName.Text);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Infromation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }
        private void Titlep()
        {
            DataGridReceipt.AutoGenerateColumns = false;
            DataGridReceipt.Refresh();
            DataGridReceipt.DataSource = null;
            DataGridReceipt.Rows.Clear();
            DataGridReceipt.ColumnCount = 16;
            DataGridReceipt.Columns[0].Name = "Itemname";
            DataGridReceipt.Columns[1].Name = "SetNo";
            DataGridReceipt.Columns[2].Name = "Ends";
            DataGridReceipt.Columns[3].Name = "BeamNo";
            DataGridReceipt.Columns[4].Name = "Length";
            DataGridReceipt.Columns[5].Name = "Gross";

            DataGridReceipt.Columns[6].Name = "T.Wt";
            DataGridReceipt.Columns[7].Name = "Net Wt";
            DataGridReceipt.Columns[8].Name = "ActualWt";
            DataGridReceipt.Columns[9].Name = "Itemid";
            DataGridReceipt.Columns[10].Name = "listid";
            DataGridReceipt.Columns[11].Name = "beamid";
            DataGridReceipt.Columns[12].Name = "Barcode";
            DataGridReceipt.Columns[13].Name = "Difference";
            DataGridReceipt.Columns[14].Name = "MillName";
            DataGridReceipt.Columns[15].Name = "MillId";
            DataGridReceipt.EditMode = DataGridViewEditMode.EditOnKeystroke;

            DataGridReceipt.Columns[0].Width = 170;
            DataGridReceipt.Columns[3].Width = 90;
            DataGridReceipt.Columns[1].Width = 50;
            DataGridReceipt.Columns[2].Width = 50;
            DataGridReceipt.Columns[4].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            DataGridReceipt.Columns[4].Width = 70;
            DataGridReceipt.Columns[5].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            DataGridReceipt.Columns[5].Width = 70;
            DataGridReceipt.Columns[6].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            DataGridReceipt.Columns[6].Width = 70;
            DataGridReceipt.Columns[7].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            DataGridReceipt.Columns[7].Width = 80;
            DataGridReceipt.Columns[8].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            DataGridReceipt.Columns[8].Width = 90;
            DataGridReceipt.Columns[9].Visible = false;
            DataGridReceipt.Columns[10].Visible = false;
            DataGridReceipt.Columns[11].Visible = false;
            DataGridReceipt.Columns[12].Visible = false;
            DataGridReceipt.Columns[15].Visible = false;
            //DataGridReceipt.Columns[13].Visible = false;
            DataGridReceipt.Columns[13].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            DataGridReceipt.Columns[13].Width = 80;

        }
        private void txtname_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable dt = getParty();
                FillGrid(dt, 1);
                Point loc = FindLocation(txtName);
                grSearch.Location = new Point(loc.X, loc.Y + 20);
                grSearch.Visible = true;
                grSearch.Text = "Party Search";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private Point FindLocation(Control ctrl)
        {
            if (ctrl.Parent is Form)
                return ctrl.Location;
            else
            {
                Point p = FindLocation(ctrl.Parent);
                p.X += ctrl.Location.X;
                p.Y += ctrl.Location.Y;
                return p;
            }
        }

        private DataTable getParty()
        {
            DataTable dt = new DataTable();
            try
            {
                dt = db.GetData(CommandType.StoredProcedure, "SP_getParty");
                bsParty.DataSource = dt;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            return dt;
        }

        private DataTable getBeam()
        {
            DataTable dt = new DataTable();
            try
            {
                dt = db.GetData(CommandType.StoredProcedure, "SP_GetBeamNo");
                bsBeam.DataSource = dt;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            return dt;
        }

        private void FillGrid(DataTable dt, int FillId)
        {
            try
            {
                DataGridCommon.DataSource = null;
                DataGridCommon.AutoGenerateColumns = false;
                if (FillId == 1)
                {
                    Fillid = 1;
                    DataGridCommon.ColumnCount = 3;
                    DataGridCommon.Columns[0].Name = "Uid";
                    DataGridCommon.Columns[0].HeaderText = "Uid";
                    DataGridCommon.Columns[0].DataPropertyName = "Uid";
                    DataGridCommon.Columns[1].Name = "Name";
                    DataGridCommon.Columns[1].HeaderText = "Name";
                    DataGridCommon.Columns[1].DataPropertyName = "Name";

                    DataGridCommon.Columns[2].Name = "Code";
                    DataGridCommon.Columns[2].HeaderText = "Code";
                    DataGridCommon.Columns[2].DataPropertyName = "Code";
                    DataGridCommon.Columns[1].Width = 250;
                    DataGridCommon.DataSource = bsParty;
                    DataGridCommon.Columns[0].Visible = false;
                }
                else if (FillId == 2)
                {
                    Fillid = 2;
                    DataGridCommon.ColumnCount = 3;
                    DataGridCommon.Columns[0].Name = "Uid";
                    DataGridCommon.Columns[0].HeaderText = "Uid";
                    DataGridCommon.Columns[0].DataPropertyName = "Uid";
                    DataGridCommon.Columns[1].Name = "Name";
                    DataGridCommon.Columns[1].HeaderText = "Name";
                    DataGridCommon.Columns[1].DataPropertyName = "GeneralName";
                    DataGridCommon.Columns[2].Name = "ShortName";
                    DataGridCommon.Columns[2].HeaderText = "ShortName";
                    DataGridCommon.Columns[2].DataPropertyName = "F2";
                    DataGridCommon.Columns[1].Width = 250;
                    DataGridCommon.Columns[0].Visible = false;
                    DataGridCommon.DataSource = bsItem;
                }
                else if (FillId == 3)
                {
                    Fillid = 3;
                    DataGridCommon.ColumnCount = 3;
                    DataGridCommon.Columns[0].Name = "Uid";
                    DataGridCommon.Columns[0].HeaderText = "Uid";
                    DataGridCommon.Columns[0].DataPropertyName = "Uid";
                    DataGridCommon.Columns[1].Name = "Name";
                    DataGridCommon.Columns[1].HeaderText = "Name";
                    DataGridCommon.Columns[1].DataPropertyName = "GeneralName";
                    DataGridCommon.Columns[2].Name = "Weight";
                    DataGridCommon.Columns[2].HeaderText = "Weight";
                    DataGridCommon.Columns[2].DataPropertyName = "BWght";
                    DataGridCommon.Columns[1].Width = 250;
                    DataGridCommon.DataSource = bsMill;
                    DataGridCommon.Columns[0].Visible = false;
                }
                else if (FillId == 4)
                {
                    Fillid = 4;
                    DataGridCommon.ColumnCount = 3;
                    DataGridCommon.Columns[0].Name = "Uid";
                    DataGridCommon.Columns[0].HeaderText = "Uid";
                    DataGridCommon.Columns[0].DataPropertyName = "Uid";
                    DataGridCommon.Columns[1].Name = "Name";
                    DataGridCommon.Columns[1].HeaderText = "Name";
                    DataGridCommon.Columns[1].DataPropertyName = "Name";
                    DataGridCommon.Columns[2].Name = "PartyType";
                    DataGridCommon.Columns[2].HeaderText = "PartyType";
                    DataGridCommon.Columns[2].DataPropertyName = "PartyType";
                    DataGridCommon.Columns[1].Width = 250;
                    DataGridCommon.DataSource = bsMill;
                    DataGridCommon.Columns[0].Visible = false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void Loadgrid()
        {
            try
            {
                conn.Close();
                conn.Open();
                Genclass.StrSrch = "";
                Genclass.FSSQLSortStr = "Docno";
                Genclass.FSSQLSortStr1 = "Docdate";
                Genclass.FSSQLSortStr2 = "SupplierName";
                Genclass.FSSQLSortStr3 = "totalqty";
                if (txtscr5.Text != "")
                {
                    if (Genclass.StrSrch == "")
                    {
                        Genclass.StrSrch = Genclass.FSSQLSortStr2 + " like '%" + txtscr5.Text + "%'";
                    }
                    else
                    {
                        Genclass.StrSrch = Genclass.StrSrch + " and " + Genclass.FSSQLSortStr2 + " like '%" + txtscr5.Text + "%'";
                    }
                }

                if (chkact.Checked == true)
                {
                    string quy = "select a.uid,a.Docno,a.Docdate,c.Name as SupplierName,a.DcNo,a.Remarks as Vechile,ISNULL(SUM(b.qtyinweight),0) as TotalQty,supplieruid from jor a inner join Jorlist b on a.Uid=b.JoRuid  inner join PartyM c on a.Supplieruid=c.uid where active=1 group by a.Docno,a.Docdate,a.uid,c.Name,supplieruid,a.DcNo,a.Remarks order by a.Uid desc";
                    Genclass.cmd = new SqlCommand(quy, conn);
                }
                else
                {
                    string quy = "select a.uid,a.Docno,a.Docdate,c.Name as SupplierName,a.DcNo,a.Remarks as Vechile,ISNULL(SUM(b.qtyinweight),0) as TotalQty,supplieruid from jor a inner join Jorlist b on a.Uid=b.JoRuid  inner join PartyM c on a.Supplieruid=c.uid where active=0 group by a.Docno,a.Docdate,a.uid,c.Name,supplieruid,a.DcNo,a.Remarks order by a.Uid desc";
                    Genclass.cmd = new SqlCommand(quy, conn);
                }
                SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
                DataTable tap = new DataTable();
                aptr.Fill(tap);
                bssearch.DataSource = tap;
                HFGP.AutoGenerateColumns = false;
                HFGP.Refresh();
                HFGP.DataSource = null;
                HFGP.Rows.Clear();
                HFGP.ColumnCount = tap.Columns.Count;
                Genclass.i = 0;
                foreach (DataColumn column in tap.Columns)
                {
                    HFGP.Columns[Genclass.i].Name = column.ColumnName;
                    HFGP.Columns[Genclass.i].HeaderText = column.ColumnName;
                    HFGP.Columns[Genclass.i].DataPropertyName = column.ColumnName;
                    Genclass.i = Genclass.i + 1;
                }
                HFGP.Columns[0].Visible = false;
                HFGP.Columns[1].Width = 90;
                HFGP.Columns[2].Width = 90;
                HFGP.Columns[3].Width = 430;
                HFGP.Columns[4].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                HFGP.Columns[6].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                HFGP.Columns[6].DefaultCellStyle.Format = "N3";
                HFGP.Columns[4].Width = 120;
                HFGP.Columns[7].Visible = false;
                HFGP.DataSource = tap;
                HFGP.Columns[2].DefaultCellStyle.Format = "yyyy'/'MM'/'dd";
                HFGP.DataSource = bssearch;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return;
            }
            finally
            {
                conn.Close();
                Genclass.cmd.Dispose();
            }
        }

        private void loadput()
        {
            conn.Close();
            conn.Open();

            if (Genclass.type == 1)
            {

                Genclass.Module.Partylistviewcont("uid", "Name", Genclass.strsql, this, txtpuid, txtName, Editpnl);
                Genclass.strsql = "select distinct uid,name from( select a.Supplieruid as uid,f.name,ISNULL(d.Qty,0)-ISNULL(e.QtyinWeight,0) as qty from JoI a inner join joilist b on a.Uid=b.JoIuid left join JoIlistMatch c on b.Uid=c.JoIlistuid  left join JoIlistOutput d on c.uid=d.JoIlistMatchuid left join jorlist  e on d.Uid=e.refuid   left join PartyM f on a.Supplieruid=f.uid   group by  a.Supplieruid,f.name,e.QtyinWeight,d.qty  having ISNULL(d.Qty,0)-ISNULL(e.QtyinWeight,0)>0) tab  ";

                Genclass.FSSQLSortStr = "Name";

            }
            else if (Genclass.type == 2)
            {

                Genclass.Module.Partylistviewcont2("uid", "itemname", "Qty", "listid", Genclass.strsql, this, txtitemid, txtItem, txtqty, txtlistid, Editpnl);
                Genclass.strsql = "select distinct uid,ItemName,qty,listid from(select d.Itemuid as uid,h.itemname,ISNULL(d.Qty,0)-ISNULL(e.QtyinWeight,0) as qty,d.uid  as listid from JoI a inner join joilist b on a.Uid=b.JoIuid left join JoIlistMatch c on b.Uid=c.JoIlistuid  left join JoIlistOutput d on c.uid=d.JoIlistMatchuid left join jorlist  e on d.Uid=e.refuid   left join PartyM f on a.Supplieruid=f.uid left join ItemM h on d.Itemuid=h.uid  where a.Supplieruid =" + txtpuid.Text + " group by d.Itemuid,h.itemname,e.QtyinWeight,d.qty,a.docno,d.uid  having ISNULL(d.Qty,0)-ISNULL(e.QtyinWeight,0)>0) tab ";

                Genclass.FSSQLSortStr = "ItemName";

            }
            else if (Genclass.type == 3)
            {

                Genclass.Module.Partylistviewcont3("uid", "beamno", "Qty", Genclass.strsql, this, txtbeamid, txtBeam, txtbeamwt, Editpnl);
                Genclass.strsql = "select distinct uid,beamno,qty from( select k.Uid,k.GeneralName as beamno,k.f1 as qty from JoI a inner join joilist b on a.Uid=b.JoIuid left join JoIlistBeam  p on b.Uid=p.JoRListUid left join JoIlistMatch c on b.Uid=c.JoIlistuid  left join JoIlistOutput d on c.uid=d.JoIlistMatchuid left join jorlist  e on d.Uid=e.refuid   left join PartyM f on a.Supplieruid=f.uid left join ItemM h on d.Itemuid=h.uid left join generalm k on p.BeamUid=k.Uid where d.Uid=" + txtlistid.Text + " and d.Itemuid=" + txtitemid.Text + "  group by d.Itemuid,h.itemname,e.QtyinWeight,d.qty,a.docno,k.uid,k.generalname,k.f1  having ISNULL(d.Qty,0)-ISNULL(e.QtyinWeight,0)>0) tab";
                Genclass.FSSQLSortStr = "beamno";

            }


            Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
            SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
            DataTable tap = new DataTable();
            aptr.Fill(tap);
            Frmlookup contc = new Frmlookup();
            DataGridView dt = (DataGridView)contc.Controls["Hfgp"];
            dt.Refresh();
            dt.ColumnCount = tap.Columns.Count;
            // bssearch.DataSource = dt;
            dt.DefaultCellStyle.Font = new Font("Arial", 10);
            dt.ColumnHeadersDefaultCellStyle.Font = new Font("Arial", 10, FontStyle.Bold);
            dt.AutoGenerateColumns = false;
            Genclass.i = 0;
            foreach (DataColumn column in tap.Columns)
            {
                dt.Columns[Genclass.i].Name = column.ColumnName;
                dt.Columns[Genclass.i].HeaderText = column.ColumnName;
                dt.Columns[Genclass.i].DataPropertyName = column.ColumnName;
                Genclass.i = Genclass.i + 1;
            }
            dt.DataSource = tap;
            dt.Columns[0].Visible = false;
            dt.Columns[1].Width = 350;
            if (Genclass.type == 2)
            {
                dt.Columns[2].Visible = false;
                dt.Columns[3].Visible = false;
            }

            contc.StartPosition = FormStartPosition.CenterScreen;
            contc.MdiParent = this.MdiParent;
            contc.Show();
            conn.Close();
        }

        private void txtitem_Click(object sender, EventArgs e)
        {
            DataTable dt = getItem();
            FillGrid(dt, 2);
            Point loc = FindLocation(txtItem);
            grSearch.Location = new Point(loc.X, loc.Y + 20);
            grSearch.Visible = true;
            grSearch.Text = "Item Search";
        }

        protected DataTable getItem()
        {
            DataTable dt = new DataTable();
            try
            {
                //dt = db.GetDataWithoutParam(CommandType.StoredProcedure, "SP_GetItem", conn);
                int TypeM_Uid = 26;
                int Active = 1;
                SqlParameter[] para = { new SqlParameter("@TypeM_Uid", TypeM_Uid), new SqlParameter("@Active", Active) };
                dt = db.GetData(CommandType.StoredProcedure, "SP_GetGeneralM", para);
                bsItem.DataSource = dt;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            return dt;
        }

        private void txtdcqty_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable dt = getBeam();
                FillGrid(dt, 3);
                Point loc = FindLocation(txtBeam);
                grSearch.Location = new Point(loc.X - 60, loc.Y + 20);
                grSearch.Visible = true;
                grSearch.Text = "Beam Search";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

        }

        private void txtitem_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectId == 0)
                {
                    bsItem.Filter = string.Format("GeneralName LIKE '%{0}%' or f2 LIKE '%{1}%' ", txtItem.Text, txtItem.Text);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void btnsave_Click(object sender, EventArgs e)
        {
            Genclass.Dtype = 220;
            int TypeUid = 220;
            if (DataGridReceipt.Rows.Count != 0)
            {
                if (btnsave.Text == "Save")
                {
                    mode = 1;
                    SqlParameter[] para = {
                        new SqlParameter("@DocNo",txtgrn.Text),
                        new SqlParameter("@DocDate",Convert.ToDateTime(dtpgrndt.Text)),
                        new SqlParameter("@DocTypeUid",Genclass.Dtype),
                        new SqlParameter("@SupplierId",txtName.Tag),
                        new SqlParameter("@Remarks",txtvehicle.Text),
                        new SqlParameter("@Status",txttotal.Text),
                        new SqlParameter("@DcNo",txtdcno.Text),
                        new SqlParameter("@DcDate",Convert.ToDateTime(dcdate.Text)),
                        new SqlParameter("@mode",mode),
                        new SqlParameter("@ReturnId",SqlDbType.Int)
                    };
                    para[9].Direction = ParameterDirection.Output;
                    int Id = db.ExecuteQuery(CommandType.StoredProcedure, "SP_JoorderRec", para, 9);
                    for (int i = 0; i < DataGridReceipt.RowCount - 1; i++)
                    {
                        if (mode == 1)
                        {
                            SQLDBHelper db1 = new SQLDBHelper();
                            SqlParameter[] paraDet = {
                            new SqlParameter("@JorUid",Id),
                            new SqlParameter("@ItemUid",Convert.ToInt32(DataGridReceipt.Rows[i].Cells[9].Value)),
                            new SqlParameter("@BeamUid",Convert.ToInt32(DataGridReceipt.Rows[i].Cells[11].Value)),
                            new SqlParameter("@QtyinWeight",Convert.ToDecimal(DataGridReceipt.Rows[i].Cells[5].Value)),
                            new SqlParameter("@QtyinMeters",Convert.ToDecimal(DataGridReceipt.Rows[i].Cells[4].Value)),
                            new SqlParameter("@SetNo",DataGridReceipt.Rows[i].Cells[1].Value),
                            new SqlParameter("@Ends",Convert.ToDecimal(DataGridReceipt.Rows[i].Cells[2].Value)),
                            new SqlParameter("@TareWt",Convert.ToDecimal(DataGridReceipt.Rows[i].Cells[6].Value)),
                            new SqlParameter("@NetWt",Convert.ToDecimal(DataGridReceipt.Rows[i].Cells[7].Value)),
                            new SqlParameter("@Remarks","0"),
                            new SqlParameter("@RefUid",Convert.ToInt32(DataGridReceipt.Rows[i].Cells[10].Value)),
                            new SqlParameter("@Barcode",DataGridReceipt.Rows[i].Cells[12].Value),
                            new SqlParameter("@mode",mode),
                            new SqlParameter("@MillUid",DataGridReceipt.Rows[i].Cells[15].Value)
                        };
                            db1.ExecuteQuery(CommandType.StoredProcedure, "SP_JoorderReclist", paraDet);
                        }
                    }
                    MessageBox.Show("Record Saved Successfully", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    mode = 2;
                    SqlParameter[] para = {
                        new SqlParameter("@DocNo",txtgrn.Text),
                        new SqlParameter("@DocDate",Convert.ToDateTime(dtpgrndt.Text)),
                        new SqlParameter("@DocTypeUid",TypeUid),
                        new SqlParameter("@SupplierId",txtName.Tag),
                        new SqlParameter("@Remarks",txtvehicle.Text),
                        new SqlParameter("@Status",txttotal.Text),
                        new SqlParameter("@DcNo",txtdcno.Text),
                        new SqlParameter("@DcDate",Convert.ToDateTime(dcdate.Text)),
                        new SqlParameter("@mode",mode),
                        new SqlParameter("@ReturnId",SqlDbType.Int),
                        new SqlParameter("@uid",Convert.ToInt32(txtgrnid.Text))
                    };
                    para[9].Direction = ParameterDirection.Output;
                    int Id = db.ExecuteQuery(CommandType.StoredProcedure, "SP_JoorderRec", para, 9);
                    for (int i = 0; i < DataGridReceipt.RowCount - 1; i++)
                    {
                        if (mode == 2)
                        {
                            SQLDBHelper db1 = new SQLDBHelper();
                            SqlParameter[] paraDet = {
                                new SqlParameter("@JorUid",Id),
                                new SqlParameter("@ItemUid",Convert.ToInt32(DataGridReceipt.Rows[i].Cells[9].Value)),
                                new SqlParameter("@BeamUid",Convert.ToInt32(DataGridReceipt.Rows[i].Cells[11].Value)),
                                new SqlParameter("@QtyinWeight",Convert.ToDecimal(DataGridReceipt.Rows[i].Cells[5].Value)),
                                new SqlParameter("@QtyinMeters",Convert.ToDecimal(DataGridReceipt.Rows[i].Cells[4].Value)),
                                new SqlParameter("@SetNo",DataGridReceipt.Rows[i].Cells[1].Value),
                                new SqlParameter("@Ends",Convert.ToDecimal(DataGridReceipt.Rows[i].Cells[2].Value)),
                                new SqlParameter("@TareWt",Convert.ToDecimal(DataGridReceipt.Rows[i].Cells[6].Value)),
                                new SqlParameter("@NetWt",Convert.ToDecimal(DataGridReceipt.Rows[i].Cells[7].Value)),
                                new SqlParameter("@Remarks","0"),
                                new SqlParameter("@RefUid",Convert.ToInt32(DataGridReceipt.Rows[i].Cells[10].Value)),
                                new SqlParameter("@Barcode",DataGridReceipt.Rows[i].Cells[12].Value),
                                new SqlParameter("@mode",mode),
                                new SqlParameter("@MillUid",DataGridReceipt.Rows[i].Cells[15].Value)
                            };
                            db1.ExecuteQuery(CommandType.StoredProcedure, "SP_JoorderReclist", paraDet, conn);
                        }
                    }
                    MessageBox.Show("Record Saved Successfully", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            SqlParameter[] para2 = { new SqlParameter("@BarType", "YarnReceipt"), new SqlParameter("@LastSNo", LastNo) };
            db.GetData(CommandType.StoredProcedure, "SP_UpdateBarcode", para2);
            Loadgrid();
            Editpnl.Visible = false;
            Genpan.Visible = true;
            panadd.Visible = true;
            btnsave.Text = "Save";
            Genclass.Module.ClearTextBox(this, Editpnl);
            DataGridReceipt.Rows.Clear();
        }

        private void butedit_Click(object sender, EventArgs e)
        {
            SqlParameter[] para = { new SqlParameter("@BarType", "YarnReceipt") };
            DataTable dt = db.GetData(CommandType.StoredProcedure, "SP_BarcodePrint", para);
            YearId = Convert.ToInt32(dt.Rows[0]["YerarId"].ToString());
            MonthId = Convert.ToInt32(dt.Rows[0]["MonthId"].ToString());
            LastNo = Convert.ToInt32(dt.Rows[0]["LastSNo"].ToString());
            BUid = Convert.ToInt32(dt.Rows[0]["Uid"].ToString());
            mode = 2;
            btnsave.Text = "Update";
            panadd.Visible = false;
            Genpan.Visible = false;
            Editpnl.Visible = true;

            conn.Open();
            conn.Close();

            int i = HFGP.SelectedCells[0].RowIndex;
            uid = HFGP.Rows[i].Cells[0].Value.ToString();
            txtgrnid.Text = HFGP.Rows[i].Cells[0].Value.ToString();
            txtgrn.Text = HFGP.Rows[i].Cells[1].Value.ToString();
            txtpuid.Text = HFGP.Rows[i].Cells[7].Value.ToString();
            txtName.Tag = HFGP.Rows[i].Cells[7].Value.ToString();
            DataGridReceipt.Refresh();
            DataGridReceipt.DataSource = null;
            DataGridReceipt.Rows.Clear();
            Chkedtact.Checked = true;
            Genclass.strsql = @"Select b.*,d.GeneralName as itemname,e.Name as suppname,b.ItemUid,b.QtyinWeight,c.GeneralName as beamno,a.Remarks as Vech,
                                a.DcNo,f.Name,b.MillUid from Jor a 
                                Inner join JorList b on a.Uid = b.JoRuid 
                                Inner join GeneralM c on b.BeamUid = c.Uid and c.TypeM_Uid = 15
                                inner join GeneralM d on b.ItemUid = d.Uid and d.TypeM_uid = 26
                                inner join PartyM e on a.Supplieruid = e.Uid
                                Left Join PartyM f on b.MillUid = f.uid
                                where a.Uid =" + txtgrnid.Text + "";
            Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
            SqlDataAdapter aptr1 = new SqlDataAdapter(Genclass.cmd);
            DataTable tap1 = new DataTable();
            aptr1.Fill(tap1);
            txtdcno.Text = tap1.Rows[0]["DcNo"].ToString();
            txtvehicle.Text = tap1.Rows[0]["Vech"].ToString();
            if (tap1.Rows.Count > 0)
            {
                Genclass.sum1 = 0;
                Genclass.sum2 = 0;
                Genclass.sum3 = 0;
                Genclass.sum4 = 0;
                Genclass.sum5 = 0;
                txttotal.Text = "";
                txttwt.Text = "";
                txtactualwt.Text = "";
                txtnetwt.Text = "";
                txtdifwt.Text = "";

                for (int k = 0; k < tap1.Rows.Count; k++)
                {
                    var index = DataGridReceipt.Rows.Add();
                    DataGridReceipt.Rows[index].Cells[0].Value = tap1.Rows[k]["itemname"].ToString();
                    DataGridReceipt.Rows[index].Cells[1].Value = tap1.Rows[k]["setno"].ToString();
                    DataGridReceipt.Rows[index].Cells[2].Value = tap1.Rows[k]["ends"].ToString();
                    DataGridReceipt.Rows[index].Cells[3].Value = tap1.Rows[k]["beamno"].ToString();
                    double sump = Convert.ToDouble(tap1.Rows[k]["qtyinmeters"].ToString());
                    DataGridReceipt.Rows[index].Cells[4].Value = sump.ToString("0.000");
                    double sump1 = Convert.ToDouble(tap1.Rows[k]["qtyinweight"].ToString());
                    DataGridReceipt.Rows[index].Cells[5].Value = sump1.ToString("0.000");
                    double sump2 = Convert.ToDouble(tap1.Rows[k]["tarewt"].ToString());
                    DataGridReceipt.Rows[index].Cells[6].Value = sump2.ToString("0.000");
                    double sump3 = Convert.ToDouble(tap1.Rows[k]["netwt"].ToString());
                    DataGridReceipt.Rows[index].Cells[7].Value = sump3.ToString("0.000");
                    DataGridReceipt.Rows[index].Cells[8].Value = tap1.Rows[k]["Remarks"].ToString();
                    DataGridReceipt.Rows[index].Cells[9].Value = tap1.Rows[k]["itemuid"].ToString();
                    DataGridReceipt.Rows[index].Cells[10].Value = tap1.Rows[k]["Uid"].ToString();
                    DataGridReceipt.Rows[index].Cells[11].Value = tap1.Rows[k]["beamuid"].ToString();
                    DataGridReceipt.Rows[index].Cells[12].Value = tap1.Rows[k]["barcode"].ToString();
                    double ui = Convert.ToDouble(DataGridReceipt.Rows[index].Cells[8].Value) - Convert.ToDouble(DataGridReceipt.Rows[index].Cells[5].Value);
                    DataGridReceipt.Rows[index].Cells[13].Value = ui.ToString("0.000");
                    DataGridReceipt.Rows[index].Cells[14].Value = tap1.Rows[k]["Name"].ToString();
                    DataGridReceipt.Rows[index].Cells[15].Value = tap1.Rows[k]["MillUid"].ToString();
                    txtName.Text = tap1.Rows[k]["suppname"].ToString();
                    Genclass.sum1 = Genclass.sum1 + Convert.ToDouble(DataGridReceipt.Rows[k].Cells[5].Value);
                    Genclass.sum2 = Genclass.sum2 + Convert.ToDouble(DataGridReceipt.Rows[k].Cells[6].Value);
                    Genclass.sum3 = Genclass.sum3 + Convert.ToDouble(DataGridReceipt.Rows[k].Cells[7].Value);
                    Genclass.sum4 = Genclass.sum4 + Convert.ToDouble(DataGridReceipt.Rows[k].Cells[8].Value);
                    Genclass.sum5 = Genclass.sum5 + Convert.ToDouble(DataGridReceipt.Rows[k].Cells[13].Value);
                }
                txttotal.Text = Genclass.sum1.ToString("0.000");
                txttwt.Text = Genclass.sum2.ToString("0.000");
                txtnetwt.Text = Genclass.sum3.ToString("0.000");
                txtactualwt.Text = Genclass.sum4.ToString("0.000");
                txtdifwt.Text = Genclass.sum5.ToString("0.000");
                //Titlep();
            }
        }

        private void DataGridCommon_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                SelectId = 1;
                int Index = DataGridCommon.SelectedCells[0].RowIndex;
                if (Fillid == 1)
                {
                    txtName.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtName.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    txtItem.Focus();
                }
                else if (Fillid == 2)
                {
                    txtItem.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtItem.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    txtMillName.Focus();
                }
                else if (Fillid == 3)
                {
                    txtBeam.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtBeam.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    txtbeamwt.Text = DataGridCommon.Rows[Index].Cells[2].Value.ToString();
                    txtlength.Focus();
                }
                else if (Fillid == 4)
                {
                    txtMillName.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtMillName.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    txtsetno.Focus();
                }
                grSearch.Visible = false;
                SelectId = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void txtGridSearch_TextChanged(object sender, EventArgs e)
        {
            if (Fillid == 1)
            {
                bsParty.Filter = string.Format("Name LIKE '%{0}%' ", txtName.Text);
            }
            else if (Fillid == 2)
            {
                bsItem.Filter = string.Format("ItemName LIKE '%{0}%' ", txtItem.Text);
            }
            else if (Fillid == 3)
            {
                bsBeam.Filter = string.Format("GeneralName LIKE '%{0}%' ", txtBeam.Text);
            }
            else if (Fillid == 4)
            {
                bsMill.Filter = string.Format("Name LIKE '%{0}%' ", txtMillName.Text);
            }
        }

        private void btnHide_Click(object sender, EventArgs e)
        {
            grSearch.Visible = false;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            SelectId = 1;
            if (DataGridCommon.Rows.Count > 0)
            {
                int Index = DataGridCommon.SelectedCells[0].RowIndex;
                if (Fillid == 1)
                {
                    txtName.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtName.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    txtItem.Focus();
                }
                else if (Fillid == 2)
                {
                    txtItem.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtItem.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    txtMillName.Focus();
                }
                else if (Fillid == 3)
                {
                    txtBeam.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtBeam.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    txtbeamwt.Text = DataGridCommon.Rows[Index].Cells[2].Value.ToString();
                    txtsetno.Focus();
                }
                else if (Fillid == 4)
                {
                    txtMillName.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtMillName.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    txtsetno.Focus();
                }
                grSearch.Visible = false;
            }
            SelectId = 0;
        }

        private void txtItem_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.F7)
                {
                    grSearch.Visible = false;
                }
                else if (e.KeyCode == Keys.F6)
                {
                    SelectId = 1;
                    if (DataGridCommon.Rows.Count > 0)
                    {
                        int Index = DataGridCommon.SelectedCells[0].RowIndex;
                        if (Fillid == 1)
                        {
                            txtName.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                            txtName.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                        }
                        else if (Fillid == 2)
                        {
                            txtItem.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                            txtItem.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                        }
                        else if (Fillid == 3)
                        {
                            txtBeam.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                            txtBeam.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                            txtbeamwt.Text = DataGridCommon.Rows[Index].Cells[2].Value.ToString();
                        }
                        else if (Fillid == 4)
                        {
                            txtMillName.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                            txtMillName.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                        }
                        grSearch.Visible = false;
                    }
                    SelectId = 0;
                }
                else if (e.KeyCode == Keys.Escape)
                {
                    grSearch.Visible = false;
                }
                else
                {
                    if (Fillid == 1)
                    {
                        bsParty.Filter = string.Format("Name LIKE '%{0}%' ", txtName.Text);
                    }
                    else if (Fillid == 2)
                    {
                        bsItem.Filter = string.Format("GeneralName LIKE '%{0}%' or F2 LIKE '%{1}%' ", txtItem.Text, txtItem.Text);
                    }
                    else if (Fillid == 3)
                    {
                        bsBeam.Filter = string.Format("GeneralName LIKE '%{0}%' ", txtBeam.Text);
                    }
                    else if (Fillid == 4)
                    {
                        bsMill.Filter = string.Format("Name LIKE '%{0}%' ", txtMillName.Text);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void DataGridCommon_KeyDown(object sender, KeyEventArgs e)
        {
            txtItem_KeyDown(sender, e);
        }

        private void buttnnxtlft_Click(object sender, EventArgs e)
        {

        }

        private void txtscr5_TextChanged(object sender, EventArgs e)
        {
            try
            {
                bssearch.Filter = string.Format("DocNo LIKE '%{0}%' or SupplierName LIKE '%{1}%' or DcNo LIKE '%{2}%'", txtscr5.Text, txtscr5.Text, txtscr5.Text);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void DataGridReceipt_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Delete)
                {
                    int Index = DataGridReceipt.SelectedCells[0].RowIndex;
                    int DUid = Convert.ToInt32(DataGridReceipt.Rows[Index].Cells[10].Value.ToString());
                    SqlParameter[] para = { new SqlParameter("@Uid", DUid) };
                    db.ExecuteQuery(CommandType.StoredProcedure, "SP_DeleteJorList", para);
                    DataGridReceipt.Rows.RemoveAt(Index);
                    DataGridReceipt.ClearSelection();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void butexit_Click(object sender, EventArgs e)
        {

        }

        private void dtpfnt_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                DateTime dte = Convert.ToDateTime(dtpfnt.Text);
                int Month = Convert.ToInt32(dte.Month.ToString("00"));
                int Year = Convert.ToInt32(dte.Year);
                string Query = "select a.uid,a.Docno,a.Docdate,c.Name as SupplierName,a.DcNo,a.Remarks as Vechile,ISNULL(SUM(b.qtyinweight), 0) as TotalQty,supplieruid from jor a inner join Jorlist b on a.Uid = b.JoRuid  inner join PartyM c on a.Supplieruid = c.uid where active = 1 and Month(DocDate) =" + Month + " and Year(DocDate)=" + Year + " group by a.Docno,a.Docdate,a.uid,c.Name,supplieruid,a.DcNo,a.Remarks order by a.Uid desc";
                SqlDataAdapter aptr = new SqlDataAdapter(Query, conn);
                DataTable tap = new DataTable();
                aptr.Fill(tap);
                bssearch.DataSource = tap;
                HFGP.AutoGenerateColumns = false;
                HFGP.Refresh();
                HFGP.DataSource = null;
                HFGP.Rows.Clear();
                HFGP.ColumnCount = tap.Columns.Count;
                Genclass.i = 0;
                foreach (DataColumn column in tap.Columns)
                {
                    HFGP.Columns[Genclass.i].Name = column.ColumnName;
                    HFGP.Columns[Genclass.i].HeaderText = column.ColumnName;
                    HFGP.Columns[Genclass.i].DataPropertyName = column.ColumnName;
                    Genclass.i = Genclass.i + 1;
                }
                HFGP.Columns[0].Visible = false;
                HFGP.Columns[1].Width = 90;
                HFGP.Columns[2].Width = 90;
                HFGP.Columns[3].Width = 430;
                HFGP.Columns[4].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                HFGP.Columns[6].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                HFGP.Columns[6].DefaultCellStyle.Format = "N3";
                HFGP.Columns[4].Width = 120;
                HFGP.Columns[7].Visible = false;
                HFGP.DataSource = tap;
                HFGP.Columns[2].DefaultCellStyle.Format = "yyyy'/'MM'/'dd";
                HFGP.DataSource = bssearch;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void txtMillName_Enter(object sender, EventArgs e)
        {
            try
            {
                DataTable dt = getPartyM();
                FillGrid(dt, 4);
                Point loc = FindLocation(txtMillName);
                grSearch.Location = new Point(loc.X, loc.Y + 20);
                grSearch.Visible = true;
                grSearch.Text = "Mill Search";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            try
            {
                //int Index = HFGP.SelectedCells[0].RowIndex;
                //Genclass.Dtype = 210;
                //Genclass.Prtid =Convert.ToInt32(HFGP.Rows[Index].Cells[0].Value.ToString());
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void txtMillName_TextChanged(object sender, EventArgs e)
        {
            try
            {
                try
                {
                    if (SelectId == 0)
                    {
                        bsMill.Filter = string.Format("Name LIKE '%{0}%'", txtMillName.Text);
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void DataGridReceipt_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                SelectId = 1;
                int Index = DataGridReceipt.SelectedCells[0].RowIndex;
                string Actual = DataGridReceipt.Rows[Index].Cells[8].Value.ToString();
                if(Actual == "0.000")
                {
                    EditMode = 1;
                    txtItem.Text = DataGridReceipt.Rows[Index].Cells[0].Value.ToString();
                    txtMillName.Text = DataGridReceipt.Rows[Index].Cells[14].Value.ToString();
                    txtsetno.Text = DataGridReceipt.Rows[Index].Cells[1].Value.ToString();
                    txtends.Text = DataGridReceipt.Rows[Index].Cells[2].Value.ToString();
                    txtBeam.Text = DataGridReceipt.Rows[Index].Cells[3].Value.ToString();
                    txtlength.Text = DataGridReceipt.Rows[Index].Cells[4].Value.ToString();
                    txtqty.Text = DataGridReceipt.Rows[Index].Cells[5].Value.ToString();
                    txtbeamwt.Text = DataGridReceipt.Rows[Index].Cells[6].Value.ToString();
                    txtvehicle.Tag = DataGridReceipt.Rows[Index].Cells[12].Value.ToString();
                    txtItem.Tag = DataGridReceipt.Rows[Index].Cells[9].Value.ToString();
                    txtBeam.Tag = DataGridReceipt.Rows[Index].Cells[11].Value.ToString();
                    txtMillName.Tag = DataGridReceipt.Rows[Index].Cells[15].Value.ToString();
                    DataGridReceipt.Rows.Remove(DataGridReceipt.Rows[Index]);
                    DataGridReceipt.ClearSelection();
                }
                else
                {
                    MessageBox.Show("Can't Edit", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
                SelectId = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void chckAll_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (chckAll.Checked == true)
                {
                    Loadgrid();
                }
                else
                {
                    dtpfnt_ValueChanged(sender, e);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        protected DataTable getPartyM()
        {
            DataTable dt = new DataTable();
            try
            {
                //dt = db.GetDataWithoutParam(CommandType.StoredProcedure, "SP_GetItem", conn);
                int Active = 1;
                SqlParameter[] para = { new SqlParameter("@Type", "Mill"), new SqlParameter("@Active", Active) };
                dt = db.GetData(CommandType.StoredProcedure, "SP_GetPartyM", para);
                bsMill.DataSource = dt;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            return dt;
        }

        private void txtqty_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    btnadd_Click(sender, e);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }
    }
}
